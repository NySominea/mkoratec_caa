<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class LanguageController extends Controller
{
    public function index($locale){
        Session::put('locale',$locale);
        return redirect()->back();
    }
}
