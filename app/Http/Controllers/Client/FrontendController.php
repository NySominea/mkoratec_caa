<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Model\ProjectCategory;
use App\Model\Career;
use App\Model\Banner;
use App\Model\Partner;
use App\Model\Event;
use App\Model\Member;
use App\Model\Province;
use App\Model\Gallery;
use App\Model\PrincipleCategory;
use App\Model\RegisterUser;

class FrontendController extends Controller
{
    public function index(){
        $banners = Banner::orderBy('id','DESC')->get();
        $partners = Partner::orderBy('id','DESC')->get();
        $events = Event::orderBy('id','DESC')->limit(4)->get();
        $galleries = Gallery::with('province')->orderBy('id','DESC')->limit(6)->get();
        $principleCategories = PrincipleCategory::with('principles')->get()->keyBy('id');
        return view('client.homepage.index',compact('banners','partners','events','galleries','principleCategories'));
    }

    public function event(){
        $latestEvent = Event::orderBy('id','DESC')->first();
        $events = Event::orderBy('id','DESC')->skip(1)->paginate(10);
        return view('client.event.index',compact('events','latestEvent'));
    }

    public function eventDetail($id){
        $event = Event::findOrFail($id);
        $events = Event::orderBy('id','DESC')->where('id','!=',$id)->limit(10)->get();
        return view('client.event.detail',compact('event','events'));
    }

    public function gallery(){
        $provinces = ['all' => trans('client.province_placeholder')] + Province::orderBy('name','ASC')->pluck('name','name')->toArray();
        $galleries = Gallery::with('province')->orderBy('location','ASC')
                    ->when(request()->province && request()->province!='all',function($q){
                        return $q->whereHas('province', function ($query) {
                            $query->where('name', '=', request()->province);
                        });
                    })
                    ->when(request()->location,function($q){
                        return $q->where('location',request()->location);
                    })->get();
        if(request()->location){
            $galleries = $galleries[0]->media()->where('collection_name', 'album')->paginate(12);
        }
            
        return view('client.gallery.index',compact('provinces','galleries'));
            
    }

    public function aboutUs(){
        $members = Member::all();
        $principleCategories = PrincipleCategory::with('principles')
                                ->get()->keyBy('id');
        return view('client.about.index',compact('members','principleCategories'));
    }

    public function registerForm(){
        return view('client.register.index');
    }
    public function register(Request $request){ 
        $this->validate(request(),[
            'kh_name' => 'required',
            'en_name' => 'required',
            'gender' => 'required',
            'age' => 'required',
            'id_number' => 'required',
            'address' => 'required',
            'education' => 'required',
            'language' => 'required',
            'position' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'against_humanity' => 'required',
            'term-condition-1' => 'required',
            'term-condition-2' => 'required',
            'photo' => 'required'
        ],[
            'kh_name.required' => trans('client.kh_name_field_validation'),
            'en_name.required' => trans('client.en_name_field_validation'),
            'id_number.required' => trans('client.id_number_field_validation'),
            'gender.required' => trans('client.gender_field_validation'),
            'age.required' => trans('client.age_field_validation'),
            'address.required' => trans('client.address_field_validation'),
            'photo.required' => trans('client.photo_field_validation'),
            'education.required' => trans('client.education_field_validation'),
            'language.required' => trans('client.language_field_validation'),
            'position.required' => trans('client.career_position_field_validation'),
            'phone.required' => trans('client.phone_field_validation'),
            'email.required' => trans('client.email_field_validation'),
            'against_humanity.required' => trans('client.against_humanity_field_validation'),
        ]); 
        
        $user = RegisterUser::create(request()->all());
        
        if($user){
            $user->addMediaFromRequest('photo')->toMediaCollection('images');
        }

        return back()->withSuccess('Congratulations! You have successfully registered as a member of Cambodia Alumni Association.');
    }
}
