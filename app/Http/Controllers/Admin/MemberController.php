<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Member;
use DB;

class MemberController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-member',   ['only' => ['index']]);
        $this->middleware('permission:add-new-member',   ['only' => ['create']]);
        $this->middleware('permission:member-modification',   ['only' => ['update','destroy']]);
    }

    public function index()
    {
        $members = Member::orderBy('id','DESC')->paginate(10);
        
        return view('admin.member.index',compact('members'));
    }

    public function create()
    {
        return view('admin.member.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'memberDropzoneImage' => 'required',
            'name' => 'required|max:100',
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.members.index')->withSuccess('You have just added a member successfully!');
    }

    public function edit($id)
    {
        $member = Member::findOrFail($id);
        return view('admin.member.add-update',compact('member'));
    }

    public function update(Request $request, $id)
    {  
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.members.index')->withSuccess('You have just updated a member successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $member = Member::find($id);
       if($member){
           DB::beginTransaction();
           try{
               if($member->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $member = isset($id) ? Member::find($id) : new Member;
            if(!$member) return redirect()->back()->withError('There is no record found!');

            $member->fill($data);
            if($member->save()){
                saveModelSingleImage($member,$data['memberDropzoneImage'],'images');
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $member;
    }
}
