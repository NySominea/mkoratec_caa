<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Event;
use DB;

class EventController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-event',   ['only' => ['index']]);
        $this->middleware('permission:add-new-event',   ['only' => ['create']]);
        $this->middleware('permission:event-modification',   ['only' => ['update','destroy']]);
    }

    public function index()
    {
        $events = Event::orderBy('id','DESC')->paginate(10);
        
        return view('admin.event.index',compact('events'));
    }

    public function create()
    {
        return view('admin.event.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title.en' => 'required'
        ],[
            'title.en.required' => 'The title is required.'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.events.index')->withSuccess('You have just added a event successfully!');
    }

    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.event.add-update',compact('event'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request,[
            'title.en' => 'required'
        ],[
            'title.en.required' => 'The title is required.'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.events.index')->withSuccess('You have just updated a event successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $event = event::find($id);
       if($event){
           DB::beginTransaction();
           try{
               if($event->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $event = isset($id) ? Event::find($id) : new Event;
            if(!$event) return redirect()->back()->withError('There is no record found!');
            $event->fill($data);
            if($event->save()){
                saveModelSingleImage($event,$data['eventDropzoneImage'],'images');
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $event;
    }
}
