<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Illuminate\Support\Facades\Hash;

class EditProfileController extends Controller
{
    public function edit($id)
     {
         $user = User::findOrFail($id);
         return view('admin.user.edit-profile',compact('user'));
     }
 
     public function update(Request $request, $id)
     {
         $this->validate($request,[
             'name' => 'required|unique:users,name,'.$id,
             'email' => 'required|unique:users,email,'.$id
         ]);
         
         if($request->password){
             $this->validate($request,[
                 'password' => 'required|confirmed|min:6',
                 'password_confirmation' => 'required',
             ]);
         }
 
         $this->saveToDB($request->all(),$id);
         return redirect()->back()->withSuccess('You have just updated a user successfully!');
     }

     public function destroy($id)
     {
        $result = false;
        $user = User::find($id);
        if($user){
            DB::beginTransaction();
            try{
                if($user->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
     }

     public function saveToDB($data, $id=null){
         DB::beginTransaction();
         try{
             $user = isset($id) ? User::find($id) : new user;
             if(!$user) return redirect()->back()->withError('There is no record found!');
    
             if($data['password']){
                 $data['password'] = Hash::make($data['password']);
             }else{
                 unset($data['password']);
             }
             $user->fill($data);
             if($user->save()){
                saveModelSingleImage($user,$data['profileDropzoneImage'],'images');
             }

             DB::commit();
         }catch(Exception $ex){
             DB::rollback();
             return redirect()->back()->withError('There was an error during operation!');
         }
         return $user;
     }
}
