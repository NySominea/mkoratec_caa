<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Principle;
use App\Model\PrincipleCategory;
use DB;

class PrincipleController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-principle',   ['only' => ['index']]);
        $this->middleware('permission:add-new-principle',   ['only' => ['create']]);
        $this->middleware('permission:gallery-principle|principle-modification',   ['only' => ['update','destroy']]);
    }

    public function index()
    {
        $principles = Principle::with('category')
                            ->orderBy('id','DESC')
                            ->orderBy('category_id','DESC')
                            ->paginate(10);

        return view('admin.principle.index',compact('principles'));
    }

    public function create()
    {
        $categories = PrincipleCategory::orderBy('name','ASC')->pluck('name','id')->toArray();
        $categories = ['0' => 'Choose Category'] + $categories;
        return view('admin.principle.add-update',compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            // 'title.en' => 'required',
            'description.en' => 'required'
        ],[
            'title.en.required' => 'The title is required.',
            'description.en.required' => 'The description is required.'
        ]);
        
        $this->saveToDB($request->all());
        return redirect()->route('admin.principles.index')->withSuccess('You have just added a principle successfully!');
    }

    public function edit($id)
    {
        $categories = PrincipleCategory::orderBy('name','ASC')->pluck('name','id')->toArray();
        $categories = ['0' => 'Choose Category'] + $categories;
        $principle = Principle::findOrFail($id);
        return view('admin.principle.add-update',compact('principle','categories'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request,[
            // 'title.en' => 'required',
            'description.en' => 'required'
        ],[
            'title.en.required' => 'The title is required.',
            'description.en.required' => 'The description is required.'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.principles.index')->withSuccess('You have just updated a principle successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $principle = Principle::find($id);
       if($principle){
           DB::beginTransaction();
           try{
               if($principle->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $principle = isset($id) ? Principle::find($id) : new Principle;
            if(!$principle) return redirect()->back()->withError('There is no record found!');

            $principle->fill($data);
            $principle->save();

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $principle;
    }
}
