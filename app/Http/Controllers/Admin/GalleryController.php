<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Gallery;
use App\Model\Province;
use DB;

class GalleryController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-gallery',   ['only' => ['index']]);
        $this->middleware('permission:add-new-gallery',   ['only' => ['create']]);
        $this->middleware('permission:gallery-modification',   ['only' => ['update','destroy']]);
    }

    public function index()
    {
        $galleries = Gallery::with('province')->orderBy('id','DESC')->paginate(10);

        return view('admin.gallery.index',compact('galleries'));
    }

    public function create()
    {
        $provinces = Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        $provinces = ['0' => 'Choose Location'] + $provinces;
        return view('admin.gallery.add-update',compact('provinces'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'galleryDropzoneImage' => 'required',
            'location' => 'required|max:100',
            'galleryMulitDropzoneImage' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.galleries.index')->withSuccess('You have just added a gallery successfully!');
    }

    public function edit($id)
    {
        $provinces = Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        $provinces = ['0' => 'Choose Location'] + $provinces;
        $gallery = Gallery::findOrFail($id);
        return view('admin.gallery.add-update',compact('gallery','provinces'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request,[
            'location' => 'required|max:100',
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.galleries.index')->withSuccess('You have just updated a gallery successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $gallery = Gallery::find($id);
       if($gallery){
           DB::beginTransaction();
           try{
               if($gallery->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $gallery = isset($id) ? Gallery::find($id) : new Gallery;
            if(!$gallery) return redirect()->back()->withError('There is no record found!');

            $gallery->fill($data);
            if($gallery->save()){
                saveModelSingleImage($gallery,$data['galleryDropzoneImage'],'images');
                saveModelMultiImage($gallery,$data['galleryMulitDropzoneImage'],'album'); 
                if(isset($id) && $data['galleryMulitDropzoneDelete']){ 
                    deleteModelMultiImage($gallery,$data['galleryMulitDropzoneDelete'],'album');
                }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $gallery;
    }
}
