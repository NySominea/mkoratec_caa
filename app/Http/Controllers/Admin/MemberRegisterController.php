<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\RegisterUser;
use PDF;

class MemberRegisterController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-register-user',   ['only' => ['index']]);
        $this->middleware('permission:delete-register-user',   ['only' => ['destroy']]);
    }
    
    public function index()
    {
        $members = RegisterUser::orderBy('id','DESC')->paginate(10);
        return view('admin.member-register.index',compact('members'));
    }

    public function show($id)
    {
        $raw = RegisterUser::findOrFail($id);
        $member = $raw->toArray();
        $member['created_at'] = date('d F Y',strtotime($member['created_at']));
        $member['photo'] = $raw->getFirstMedia('images') ? $raw->getFirstMedia('images')->geturl() : '';
        return response()->json(['success' => true, 'member' => $member]);
    }

    public function downloadPdf($id)
    {
        $member = RegisterUser::findOrFail($id);
        view()->share('member',$member); 
        // return view('admin.member-register.pdf',compact('member')); 
        $pdf = PDF::loadView('admin.member-register.pdf', ['member' => $member]);
        return $pdf->download('Member-'.$member->en_name.'.pdf');
    }

    public function destroy($id)
    {
       $result = false;
       $member = RegisterUser::find($id);
       if($member){
           DB::beginTransaction();
           try{
               if($member->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }
}
