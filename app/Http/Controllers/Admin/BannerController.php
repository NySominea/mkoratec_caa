<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use DB;

class BannerController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-banner',   ['only' => ['index']]);
        $this->middleware('permission:add-new-banner',   ['only' => ['create']]);
        $this->middleware('permission:banner-modification',   ['only' => ['update','destroy']]);
    }

    public function index()
    {   
        $banners = Banner::orderBy('id','DESC')->paginate(10);
        return view('admin.banner.index',compact('banners'));
    }

    public function create()
    {
        return view('admin.banner.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'bannerDropzoneImage' => 'required',
            'description' => 'max:200'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.banners.index')->withSuccess('You have just added a banner successfully!');
    }

    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('admin.banner.add-update',compact('banner'));
    }

    public function update(Request $request, $id)
    {  
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.banners.index')->withSuccess('You have just updated a banner successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $banner = Banner::find($id);
       if($banner){
           DB::beginTransaction();
           try{
               if($banner->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $banner = isset($id) ? Banner::find($id) : new Banner;
            if(!$banner) return redirect()->back()->withError('There is no record found!');

            $banner->fill($data);
            if($banner->save()){
                saveModelSingleImage($banner,$data['bannerDropzoneImage'],'images');
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $banner;
    }
}
