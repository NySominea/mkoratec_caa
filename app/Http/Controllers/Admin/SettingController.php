<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;
use DB;

class SettingController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-setting',   ['only' => ['index']]);
        $this->middleware('permission:setting-modification',   ['only' => ['store','destroy']]);
    }

    public function index()
    {   
        return view('admin.setting.index');
    }

    public function store(Request $request)
    {   
        $data = $request->all();
        $keys = settingKeys();
        $imageKey = [
            'logo',
            'animated_logo',
            'member_banner',
            'about_us_banner',
            'register_banner',
            'gallery_banner',
            'event_banner',
            'footer_banner'
        ];
        
        foreach($keys as $key){
            if(in_array($key,array_keys($data)) || in_array($key,$imageKey)){
                $r = [
                    'key' => $key,
                    'value' => isset($data[$key]) ? $data[$key] : '',
                ];
                
                $row = Setting::where(['key' => $key])->first();
                if($row){ 
                    if(in_array($key,$imageKey)){ 
                        $inputImageKey = $key.'Image';
                        if(isset($data[$inputImageKey]) && $data[$inputImageKey]){
                            $row->update($r);
                            saveModelSingleImage($row,$data[$inputImageKey],'images');
                        }
                    }else{
                        $row->update($r);
                    }
                }else{
                    Setting::create($r);
                }
            }
        } 
        
        return redirect()->route('admin.settings.index')->withSuccess('You have just updated the setting successfully.');
    }
}
