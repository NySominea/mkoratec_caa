<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrincipleCategory extends Model
{
    protected $fillable = ['name'];
    public function principles(){
        return $this->hasMany(Principle::class,'category_id');
    }
}
