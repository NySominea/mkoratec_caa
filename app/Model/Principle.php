<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Principle extends BaseModel
{
    use HasTranslations;
    protected $fillable = ['title','description','category_id'];
    public $translatable = ['title','description'];

    public function category(){
        return $this->belongsTo(PrincipleCategory::class,'category_id','id');
    }
}
