<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class RegisterUser extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['en_name','kh_name','address','age','id_number',
                            'gender','education','language', 'position','phone','facebook',
                            'email','against_humanity'
                        ];

    protected $casts = [
        'language' => 'array',
    ];
}
