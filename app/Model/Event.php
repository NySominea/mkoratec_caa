<?php

namespace App\Model;

// use App\Model\BaseModel;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Spatie\Image\Manipulations;

class Event extends BaseModel implements HasMedia
{
    use HasMediaTrait,HasTranslations;
    protected $fillable = ['title','description'];
    public $translatable = ['title','description'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->format(Manipulations::FORMAT_JPG)
            ->crop(Manipulations::CROP_CENTER, 600, 400)
            ->quality(80)
            ->optimize();
    }
}
