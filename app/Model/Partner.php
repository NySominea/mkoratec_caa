<?php

namespace App\Model;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;

class Partner extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ['name','link'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->format(Manipulations::FORMAT_PNG)
            ->crop(Manipulations::CROP_CENTER, 200, 200)
            ->quality(80)
            ->optimize();
    }
}
