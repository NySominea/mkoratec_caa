<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Image\Manipulations;

class Gallery extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ['location','province_id'];

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->format(Manipulations::FORMAT_JPG)
            ->crop(Manipulations::CROP_CENTER, 450, 300)
            ->quality(80)
            ->optimize();
    }
}
