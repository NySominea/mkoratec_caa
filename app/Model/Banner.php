<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Translatable\HasTranslations;
use Spatie\Image\Manipulations;

class Banner extends Model implements HasMedia
{
    use HasMediaTrait,HasTranslations;
    protected $fillable = ['title','description','link'];
    public $translatable = ['title','description'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->format(Manipulations::FORMAT_JPG)
            ->crop(Manipulations::CROP_CENTER, 2000, 800)
            ->quality(80)
            ->optimize();
    }
}
