<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = []; 
        foreach(permissions() as $m){
            foreach($m['permissions'] as $index => $p){ 
                $permissions[] = $p['value'];
                Permission::create(['name' => $p['value']]);
            }
        }

        $role = Role::create(['name' => 'Super Administrator']);
        $role->syncPermissions($permissions);

        $user = User::whereId(1)->first();
        if($user){
            $user->syncRoles([$role->name]);
        }
    }
}
