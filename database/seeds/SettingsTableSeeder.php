<?php

use Illuminate\Database\Seeder;
use App\Model\Province;
use App\Model\PrincipleCategory;
use App\Model\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            'Battambang','Banteay Meanchey','Kampong Cham','Kampong Chhang','Kampong Speu','Kampong Thom',
            'Kampot','Kandal','Koh Kong','Kratie','Mondul Kiri','Phnom Penh','Preah Vihear','Prey Veng',
            'Pursat','Ranthanak Kiri','Siem Reap','Preah Sihanouk','Stung Treng','Svay Rieng','Takeo','Oddar Meanchey','Kaeb','Pailin','Tboung Khmum'
        ];
        sort($provinces);
        $provincesData = [];
        foreach($provinces as $p){ 
            $provincesData[] = ['name' => $p]; 
        }
        Province::insert($provincesData);

        $principleCategories = [
            ['name' => 'Our Visions'],
            ['name' => 'Our Missions'],
            ['name' => 'Our Core Values']
        ];
        PrincipleCategory::insert($principleCategories);

        $settings = [];
        foreach(settingKeys() as $key){
            $settings[] = ['key' => $key];
        }
        Setting::insert($settings);
    }
}
