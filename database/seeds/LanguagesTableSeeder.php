<?php

use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(frontendLanguage() as $key => $row){
            $language = LanguageLine::where('key',$key)->first(); 
            if($language){
                $language->update([
                    'text' => $row,
                ]);
            }else{
                LanguageLine::create([
                    'group' => 'client',
                    'key' => $key,
                    'text' => $row,
                ]);
            }
        }
    }
}
