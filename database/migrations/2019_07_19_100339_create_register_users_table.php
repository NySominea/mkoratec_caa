<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('en_name');
            $table->string('kh_name');
            $table->enum('gender',['Male','Female']);
            $table->integer('age');
            $table->string('id_number',30);
            $table->string('education',100);
            $table->string('language',100);
            $table->string('position',100);
            $table->string('phone',50);
            $table->string('email',50);
            $table->string('facebook')->nullable();
            $table->boolean('against_humanity');
            $table->string('address',300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_users');
    }
}
