@if($errors->has($key))
<p style="color:red !important"><small id="{{ isset($id) ? $id : ''}}">{{ $errors->first($key) }}</small></p>
@endif