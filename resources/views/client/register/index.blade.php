@extends('client.layouts.master')

@section('title') 
    Register {{ isset($settings,$settings['site_name']) ? '- '.$settings['site_name']->value : ''}}
@endsection

@section('content')
@php $image = isset($settings,$settings['register_banner']) ? $settings['register_banner']->getMedia('images')->first() : null @endphp 
<div class="page-banner" style="background-image: url({{ isset($image) && $image ? $image->getUrl() : '/client/images/img-page-header-2.jpg' }})">
    <div class="overlay">
        <div class="title">{{ trans('client.register_menu') }}</div>
    </div>
</div>
<div id="register">
    <div class="container-fluid our-story">
        <div class="row">
            <div class="offset-lg-2 col-lg-8">
                <div class="card shadow border-0">
                    <div class="card-body p-md-5 p-sm-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="section-title">{{ trans('client.member_registration_label') }}</div>
                                </div>
                            </div>
                            @if(session()->has('success'))
                            <div class="alert alert-success mb-2" role="alert">
                                <strong>{{ session()->get('success')}}</strong> 
                            </div>
                            @endif
                            {{-- <div class="mb-4 text-center text-danger">*** {{ trans('client.fill_information_below_validation') }} ***</div> --}}
                            <form class="needs-validation" action="{{route('client.register')}}" method="POST" novalidate enctype='multipart/form-data'>
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.kh_name_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <input required type="text" value="{{old('kh_name')}}" class="form-control {{$errors->has('kh_name') ? 'is-invalid' : '' }}" name="kh_name" placeholder="{{ trans('client.kh_name_placeholder') }}">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('kh_name') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.en_name_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="text" value="{{old('en_name')}}" class="form-control {{$errors->has('en_name') ? 'is-invalid' : '' }}" name="en_name" placeholder="{{ trans('client.en_name_placeholder') }}">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('en_name') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.gender_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <div class="form-check d-inline mr-3">
                                                    <input class="form-check-input" type="radio" name="gender" value="Male" {{old('gender') == 'Male' ? 'checked' : 'checked'}}>
                                                    <label class="form-check-label" >
                                                        {{ trans('client.male_label') }}
                                                    </label>
                                                </div>
                                                <div class="form-check d-inline">
                                                    <input class="form-check-input" type="radio" name="gender" value="Femlale" {{old('gender') == 'Female' ? 'checked' : ''}}>
                                                    <label class="form-check-label" >
                                                        {{ trans('client.female_label') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.age_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="number" name="age" value="{{old('age')}}" class="form-control {{$errors->has('age') ? 'is-invalid' : '' }}" placeholder="{{ trans('client.age_placeholder') }}">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('age') }}
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.id_number_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="text" name="id_number" value="{{old('id_number')}}" class="form-control {{$errors->has('id_number') ? 'is-invalid' : '' }}" placeholder="{{ trans('client.id_number_placeholder') }}">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('id_number') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.address_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <div class="input-group">
                                                    <textarea name="address" class="form-control {{$errors->has('address') ? 'is-invalid' : '' }} w-sm-100" placeholder="{{ trans('client.address_placeholder') }}" rows="5">{{old('address')}}</textarea>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('address') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.photo_4x6') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input {{$errors->has('photo') ? 'is-invalid' : '' }}" name="photo" accept="image/x-png,image/gif,image/jpeg">
                                                        <label class="custom-file-label" for="customFile">{{ trans('client.choose_photo_label') }}</label>
                                                    </div>
                                                </div>
                                                @if($errors->has('photo'))
                                                <div class="invalid-feedback d-block"> 
                                                    {{ $errors->first('photo') }}
                                                </div>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="section-title h5 mt-3">{{ trans('client.short_biography_label') }} <span class="text-danger">*</span></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.education_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <select name="education" class="form-control {{$errors->has('education') ? 'is-invalid' : '' }}">
                                                    <option value="" disabled selected>{{ trans('client.education_placeholder') }}</option>
                                                    @foreach(educationLevel() as $item)
                                                        <option value="{{$item}}" {{ old('education') == $item ? 'selected' : '' }}>{{$item}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('education') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.language_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <select name="language[]" data-placeholder="{{ trans('client.language_placeholder') }}" class="form-control languages {{$errors->has('language') ? 'is-invalid' : '' }}" multiple="multiple" data-live-search="true">
                                                    @foreach(languageList() as $item)
                                                        <option value="{{$item}}" {{ in_array($item,old('language')?:[]) ? 'selected' : '' }}>{{$item}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('language') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.career_position_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="text" name="position" value="{{old('position')}}" class="form-control {{$errors->has('position') ? 'is-invalid' : '' }}" placeholder="Position">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('position') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.phone_label') }} <span class="text-danger">*</span></label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="text" name="phone" value="{{old('phone')}}" class="form-control {{$errors->has('phone') ? 'is-invalid' : '' }}" placeholder="{{ trans('client.phone_placeholder') }}">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('phone') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.email_label') }} </label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="email" name="email" value="{{old('email')}}" class="form-control {{$errors->has('email') ? 'is-invalid' : '' }}" placeholder="{{ trans('client.optional_label') }}">
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('email') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-form-label">{{ trans('client.facebook_label') }}</label>
                                            <div class="col-md-8 col-sm-8">
                                                <input type="text" name="facebook" value="{{old('facebook')}}" class="form-control" placeholder="{{ trans('client.optional_label') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label d-block">{{ trans('client.against_humanity_sentence') }} <span class="text-danger">*</span></label>
                                            <div class="form-check d-inline mr-4">
                                                <input class="form-check-input" type="radio" name="against_humanity" value="1" {{old('against_humanity') == '1' ? 'checked' : 'checked'}}>
                                                <label class="form-check-label" >
                                                    {{ trans('client.yes_against_humanity_label') }}
                                                </label>
                                            </div>
                                            <div class="form-check d-inline">
                                                <input class="form-check-input" type="radio" name="against_humanity" value="0"  {{old('against_humanity') == '0' ? 'checked' : ''}}>
                                                <label class="form-check-label" >
                                                    {{ trans('client.no_against_humanity_label') }}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="mt-5">
                                            <label for="">{{ trans('client.term_condition_label') }}</label>
                                            @if($errors->has('term-condition-1') || $errors->has('term-condition-2'))
                                                <div class="mb-3 text-danger"> {{ trans('client.agree_condition_below_validation') }}</div>
                                            @endif
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="term-condition-1">
                                                    <label class="form-check-label">
                                                        {{ trans('client.register_condition_first_sentence') }}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group mt-3">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="term-condition-2">
                                                    <label class="form-check-label">
                                                        {{ trans('client.register_condition_second_sentence') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        

                                        <div class="text-center mt-4">
                                            <a href="{{ route('client.homepage') }}" class="btn btn-secondary-color btn-outline-dark bg-light text-dark">{{ trans('client.discard_button') }}</a>
                                            <button type="submit" class="btn btn-secondary-color">{{ trans('client.register_button') }}</button>
                                        </div>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 sidebar d-none d-lg-block">
                <div class="card item shadow border-0">
                    <div class="card-header text-center">
                        {{ trans('client.today_view_label') }}
                    </div>
                    <div class="card-body">
                        @if(isset($settings,$settings['facebook']) && !empty($settings['facebook']))
                            <img width="35px;" src="{{asset('client/images/icon/ic-person.svg')}}" alt="Users">
                        @endif
                        {{ $visitors }}
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>
@endsection

@section('custom-js')
<script>
    $('#about .member .owl-carousel').owlCarousel({
        loop: true,
        margin: 40,
        autoplay: true,
        autoplayTimeout: 3000,
        slideTransition: 'linear',
        smartSpeed:3000,
        // autoplayHoverPause: true,
        dots:false,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:2,
                margin:60
            },
            430:{
                items:2,
                margin:40
            },
            576:{
                items:3,
                margin:50
            },
            768:{
                items:4,
                margin:50
            },
            991:{
                items:3,
                margin:50
            },
            1200:{
                items:4,
                margin:60
            }
        }
    });
    $('.languages').select2({
        tags: true,
        placeholder: $(this).data('placeholder'),
        allowClear: true
    });
    $(document).ready(function(){
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });
    });
</script>
@endsection