@extends('client.layouts.master')

@section('title')
    About Us {{ isset($settings,$settings['site_name']) ? '- '.$settings['site_name']->value : ''}}
@endsection

@section('content')
@php $image = isset($settings,$settings['about_us_banner']) ? $settings['about_us_banner']->getMedia('images')->first() : null @endphp
<div class="page-banner" style="background-image: url({{ isset($image) && $image ? $image->getUrl() : '/client/images/img-page-header-2.jpg' }})">
    <div class="overlay">
        <div class="title">{{ trans('client.about_menu') }}</div>
    </div>
</div>
<div id="about">
    <div class="container-fluid our-story">
        <div class="row">
            <div class="offset-lg-2 col-lg-8">
                <div class="card shadow border-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="section-title">{{ trans('client.story_label') }}</div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-xl-12 px-md-5 py-md-3">
                                <div class="card">
                                    <div class="card-body text-color text-justify p-4 ">
                                        @if(isset($settings,$settings['our_story_description']))
                                            {!! $settings['our_story_description']->value !!}
                                        @else
                                            {!! isset($settings,$settings['site_name']) ? $settings['site_name']->value : '' !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 sidebar d-none d-lg-block">
                <div class="card item shadow border-0">
                    <div class="card-body text-left">
                        <div class="account mb-3">
                            <p class="title">{{ trans('client.bank_name_label') }}</p>
                            <h5 class="text">{{ isset($settings,$settings['bank_name']) ? $settings['bank_name']->value : ''}}</h5>
                        </div>
                        <div class="account mb-3">
                            <p class="title">{{ trans('client.account_name_label') }}</p>
                            <h5 class="text">{{ isset($settings,$settings['bank_account_name']) ? $settings['bank_account_name']->value : ''}}</h5>
                        </div>
                        <div class="account">
                            <p class="title">{{ trans('client.account_number_label') }}</p>
                            <h5 class="text">{{ isset($settings,$settings['bank_account_number']) ? $settings['bank_account_number']->value : ''}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php $image = isset($settings,$settings['member_banner']) ? $settings['member_banner']->getMedia('images')->first() : null @endphp
    <div class="member" style="background-image:url({{ $image ? $image->getUrl() : '/client/images/img-our-member.jpg'}})">
        <div class="overlay">
            <div class="row mb-5 mt-3">
                <div class="col-lg-8 offset-lg-2">
                    <div class="title">
                        {{ trans('client.member_label') }}
                    </div>
                    @if(isset($members) && $members->count() > 0)
                    <div class="owl-carousel owl-theme">
                        @foreach($members as $key => $row)
                        <div class="item">
                            @php $image = $row->getMedia('images')->first(); @endphp
                            <img class="rounded-circle" src="{{$image ? $image->getUrl() : ''}}" class="" alt="">
                            <div class="info">
                                <div class="name">{{ $row->name }}</div>
                                <div class="position">{{ $row->position }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container member-signin">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-sm-4 p-md-5">
                        <div class="description text-justify text-color">
                            @if(isset($settings,$settings['register_description']))
                                {!! $settings['register_description']->value !!}
                            @else
                                {{ isset($settings,$settings['site_name']) ? $settings['site_name']->value : ''}}
                            @endif
                        </div>
                        <div class="text-center pt-5">
                            <a href="{{route('client.register')}}" class="btn btn-secondary-color">{{ trans('client.register_button') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($principleCategories[1]) && $principleCategories[1]->count() > 0)
    <div class="vision container pt-5">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">{{ trans('client.vision_label') }}</div>
            </div>
        </div>
        <div class="row">
            @foreach($principleCategories[1]->principles as $index => $row)
            <div class="col-md-12 item">
                <div class="title text-center border-0">
                    {{-- <span class="no">{{ $index + 1 }}</span>  --}}
                    <span class="text">{{ $row->description }}</span>
                </div>
                {{-- <div class="description">{{ $row->description }}</div> --}}
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @if(isset($principleCategories[2]) && $principleCategories[2]->count() > 0)
    <div class="vision container pt-5">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">{{ trans('client.mission_label') }}</div>
            </div>
        </div>
        <div class="row">
            @foreach($principleCategories[2]->principles as $index => $row)
            <div class="col-md-6 item">
                <div class="title">
                    <span class="no">{{ $index + 1 }}</span>
                    <span class="text">{{ $row->title }}</span>
                </div>
                <div class="description">{{ $row->description }}</div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @if(isset($principleCategories[3]) && $principleCategories[3]->count() > 0)
    <div class="vision container pt-5">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">{{ trans('client.core_value_label') }}</div>
            </div>
        </div>
        <div class="row">
            @foreach($principleCategories[3]->principles as $index => $row)
            <div class="col-md-6 item">
                <div class="title">
                    <span class="no">{{ $index + 1 }}</span>
                    <span class="text">{{ $row->title }}</span>
                </div>
                <div class="description">{{ $row->description }}</div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</div>
@endsection

@section('custom-js')
<script>
    $('#about .member .owl-carousel').owlCarousel({
        loop: true,
        margin: 40,
        autoplay: true,
        autoplayTimeout: 3000,
        slideTransition: 'linear',
        smartSpeed:3000,
        // autoplayHoverPause: true,
        dots:false,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:2,
                loop:$('.owl-carousel').children().length > 2 ? true : false,
                margin: $('.owl-carousel').children().length > 1 ? 60 : $('.owl-carousel').children().length == 1 ? 0 : 30
            },
            430:{
                items:2,
                loop:$('.owl-carousel').children().length > 2 ? true : false,
                margin: $('.owl-carousel').children().length > 1 ? 40 : $('.owl-carousel').children().length == 1 ? 0 : 15
            },
            576:{
                items:3,
                loop:$('.owl-carousel').children().length > 3 ? true : false,
                margin: $('.owl-carousel').children().length > 2 ? 50 : $('.owl-carousel').children().length == 1 ? 0 : 30
            },
            768:{
                items:4,
                loop:$('.owl-carousel').children().length > 4 ? true : false,
                margin: $('.owl-carousel').children().length > 3 ? 50 : $('.owl-carousel').children().length == 1 ? 0 : 30
            },
            991:{
                items:3,
                loop:$('.owl-carousel').children().length > 3 ? true : false,
                margin: $('.owl-carousel').children().length > 2 ? 50 : $('.owl-carousel').children().length == 1 ? 0 : 30
            },
            1200:{
                items:4,
                loop:$('.owl-carousel').children().length > 4 ? true : false,
                margin: $('.owl-carousel').children().length > 3 ? 60 : $('.owl-carousel').children().length == 1 ? 0 : 30
            }
        }
    });
</script>
@endsection
