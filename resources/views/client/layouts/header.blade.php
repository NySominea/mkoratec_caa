
<nav id="navbar" class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid d-lg-none navbar-top">
        <ul class="navbar-nav navbar-top-menu ml-auto flex-row border-none mx-auto">
            @if(isset($settings,$settings['facebook']) && $settings['facebook']->value != '')
            <li class="nav-item"><a target="_BLANK" href="{{$settings['facebook']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-facebook-navbar.svg')}}" alt="Facebook"></a></li>
            @endif

            @if(isset($settings,$settings['youtube']) && $settings['youtube']->value != '')
            <li class="nav-item"><a target="_BLANK" href="{{$settings['youtube']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-youtube-navbar.svg')}}" alt="Youtube"></a></li>
            @endif

            @if(isset($settings,$settings['linkedin']) && $settings['linkedin']->value != '')
            <li class="nav-item"><a target="_BLANK" href="{{$settings['linkedin']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-linkedin-navbar.svg')}}" alt="Linked In"></a></li>
            @endif

            @if(isset($settings,$settings['instagram']) && $settings['instagram']->value != '')
            <li class="nav-item"><a target="_BLANK" href="{{$settings['instagram']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-instagram-navbar.svg')}}" alt="instagram"></a></li>
            @endif
            
            <li class="nav-item language">
                <a href="#" class="text-muted text-decoration-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span>
                        @if(app()->getLocale() == 'en') English
                        @elseif(app()->getLocale() == 'kh')  ភាសាខ្មែរ
                        @elseif(app()->getLocale() == 'zh')  中文
                        @endif
                    </span>  
                    <i class="fa fa-caret-down"></i>
                </a>
                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="z-index:99999;min-width:8.5rem">
                    @if(app()->getLocale() != 'kh') 
                    <a class="dropdown-item mr-1" href="{{route('client.set-locale','kh')}}"><img width="20px" src="{{asset('client/images/kh.jpeg')}}"> ភាសាខ្មែរ</a>
                    @endif
                    @if(app()->getLocale() != 'en') 
                        <a class="dropdown-item mr-1" href="{{route('client.set-locale','en')}}"><img width="20px" src="{{asset('client/images/en.jpeg')}}"> English</a>
                    @endif
                    @if(app()->getLocale() != 'zh')
                    <a class="dropdown-item mr-1" href="{{route('client.set-locale','zh')}}"><img width="20px" src="{{asset('client/images/zh.jpeg')}}"> 中文</a>
                    @endif
                </div>
            </li>
            <li class="nav-item"><button class="btn btn-main btn-sm"  data-toggle="modal" data-target="#donate">DONATE</button></li>
        </ul>
    </div>
    <div class="container-fluid">
        @php $image = isset($settings,$settings['animated_logo']) && $settings['animated_logo'] != '' ? $settings['animated_logo']->getMedia('images')->first() : null; @endphp
        <a class="navbar-brand" href="{{url('/')}}"><img src="{{ $image ? $image->getUrl() : '/client/images/logo.png'}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse flex-md-column" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto small d-none d-lg-flex">
                @if(isset($settings,$settings['facebook']) && $settings['facebook']->value != '')
                <li class="nav-item"><a target="_BLANK" href="{{$settings['facebook']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-facebook-navbar.svg')}}" alt="Facebook"></a></li>
                @endif

                @if(isset($settings,$settings['youtube']) && $settings['youtube']->value != '')
                <li class="nav-item"><a target="_BLANK" href="{{$settings['youtube']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-youtube-navbar.svg')}}" alt="Youtube"></a></li>
                @endif

                @if(isset($settings,$settings['linkedin']) && $settings['linkedin']->value != '')
                <li class="nav-item"><a  target="_BLANK" href="{{$settings['linkedin']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-linkedin-navbar.svg')}}" alt="Linked In"></a></li>
                @endif

                @if(isset($settings,$settings['instagram']) && $settings['instagram']->value != '')
                <li class="nav-item"><a target="_BLANK" href="{{$settings['instagram']->value}}"><img width="30px;" src="{{asset('client/images/icon/ic-instagram-navbar.svg')}}" alt="instagram"></a></li>
                @endif

                <li class="nav-item language">
                    <a href="#" class="text-muted text-decoration-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="d-inline d-sm-none d-md-inline">
                            @if(app()->getLocale() == 'en') English
                            @elseif(app()->getLocale() == 'kh')  ភាសាខ្មែរ
                            @elseif(app()->getLocale() == 'zh')  中文
                            @endif
                        </span>  
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="z-index:99999;min-width:8.5rem">
                        @if(app()->getLocale() != 'kh') 
                        <a class="dropdown-item mr-1" href="{{route('client.set-locale','kh')}}"><img width="20px" src="{{asset('client/images/kh.jpeg')}}"> ភាសាខ្មែរ</a>
                        @endif
                        @if(app()->getLocale() != 'en') 
                            <a class="dropdown-item mr-1" href="{{route('client.set-locale','en')}}"><img width="20px" src="{{asset('client/images/en.jpeg')}}"> English</a>
                        @endif
                        @if(app()->getLocale() != 'zh')
                        <a class="dropdown-item mr-1" href="{{route('client.set-locale','zh')}}"><img width="20px" src="{{asset('client/images/zh.jpeg')}}"> 中文</a>
                        @endif
                    </div>
                </li>
                <li class="nav-item"><button class="btn btn-main btn-sm"  data-toggle="modal" data-target="#donate">{{ trans('client.donate_menu') }} </button></li>
            </ul>
            <ul class="navbar-nav navbar-menu ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('client.homepage')}}">{{ trans('client.home_menu') }} <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ in_array(Route::currentRouteName(),['client.event-detail']) ? 'active' : '' }}" href="{{route('client.event')}}">{{ trans('client.event_menu') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('client.gallery')}}">{{ trans('client.gallery_menu') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('client.register')}}">{{ trans('client.register_menu') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('client.about')}}">{{ trans('client.about_menu') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="modal fade" id="donate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <img width="100%" src="{{asset('client/images/under-construction.jpg')}}" alt="">
      </div>
    </div>
</div>