@if(isset($settings,$settings['footer_banner']) && $settings['footer_banner']  != '')
   @php $image = $settings['footer_banner']->getMedia('images')->first() @endphp 
@endif
<div class="footer" style="background-image:url({{ isset($image) && $image ? $image->getUrl() : '/client/images/img-footer-background.jpg' }}">
    <div class="overlay">
        <div class="container">
            <div class="row">
                @if(isset($settings,$settings['logo']) && $settings['logo'] != '')
                    @php $image = $settings['logo']->getMedia('images')->first() @endphp
                    @if($image)
                    <div class="col-lg-12 text-center">
                        <img src="{{ $image->getUrl() }}" style="max-width:100px" alt="{{ isset($settings,$settings['site_name']) ? $settings['site_name']->value : '' }}">
                    </div>
                    @endif
                @endif

                <div class="col-lg-12 mt-3 text-center">
                    <div class="info">
                        <div class="row">
                            @if(isset($settings,$settings['phone']) && $settings['phone'] != '')
                                <div class="col-md-3 mb-3">
                                    <img width="25px" src="{{asset('client/images/icon/ic-phone.svg')}}" alt="Phone"> 
                                    <div class="mt-2 text-white"><span class="text-white">{{$settings['phone']->value}}</span></div>
                                </div>
                            @endif

                            @if(isset($settings,$settings['address']) && $settings['address'] != '')
                                <div class="col-md-6 mb-3">
                                    <img width="25px" src="{{asset('client/images/icon/ic-address.svg')}}" alt="Address"> 
                                    <div class="mt-2 text-white"><span>{{$settings['address']->value}}</span></div>
                                </div>
                            @endif

                            @if(isset($settings,$settings['email']) && $settings['email'] != '')
                                <div class="col-md-3">
                                    <img width="25px" src="{{asset('client/images/icon/ic-email.svg')}}" alt="Email"> 
                                    <div class="mt-2 text-white"><span>{{$settings['email']->value}}</span></div></li>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="social-media text-center">
                        @if(isset($settings,$settings['facebook']) && $settings['facebook']->value != '')
                            <a href="{{$settings['facebook']->value}}" class="mx-3" target="_BLANK"><img width="25px" src="{{asset('client/images/icon/ic-facebook.svg')}}" alt="Facebook"></a>
                        @endif
                        @if(isset($settings,$settings['linkedin']) && $settings['linkedin']->value != '')
                            <a href="{{$settings['linkedin']->value}}" class="mx-3" target="_BLANK"><img width="25px" src="{{asset('client/images/icon/ic-linkedin.svg')}}" alt="Linked In"></a>
                        @endif
                        @if(isset($settings,$settings['youtube']) && $settings['youtube']->value != '')
                            <a href="{{$settings['youtube']->value}}" class="mx-3" target="_BLANK"><img width="25px" src="{{asset('client/images/icon/ic-youtube.svg')}}" alt="Youtube"></a>
                        @endif
                        @if(isset($settings,$settings['instagram']) && $settings['instagram']->value != '')
                            <a href="{{$settings['instagram']->value}}" class="mx-3" target="_BLANK"><img width="25px" src="{{asset('client/images/icon/ic-instagram.svg')}}" alt="Instagram"></a>
                        @endif
                    </div>
                </div>
                
            </div>
        </div> 
        
        <div class="container-fluid p-0">
            @if(isset($settings,$settings['copy_right']) && $settings['copy_right'] != '')
            <div class="">
                <div class="copy-right text-center">
                    <span>{!! $settings['copy_right']->value !!}</span>
                    <span class="">| Powered By <a href="http://mkoratec.com" class="text-decoration-none" style="color:#E30413" target="_BLANK"><b>Mkoratec</b></a></span>        
                </div>
            </div>
            @endif
        </div>
    </div>  

    {{-- <span id="siteseal" class="d-xs-none d-md-block" style="position:absolute;bottom:0px;left:0px;"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=rGhPdVOYr3FfbX4TCDHwnOX9IrgNVR8AZKXHhFj8sINdCQq6gX2iLIreWbDs"></script></span> --}}
</div>
