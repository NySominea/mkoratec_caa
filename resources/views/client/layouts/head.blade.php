
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>
    @yield('title')
</title>
@php $image = isset($settings['logo']) ? $settings['logo']->getFirstMedia('images') : null;  @endphp
<link rel="icon" href="{{$image ? $image->getUrl() : '/client/images/logo.png'}}">
<link rel="apple-touch-icon" href="{{$image ? $image->getUrl() : '/client/images/logo.png'}}">

<meta name="description" content="{{ isset($settings,$settings['seo_description']) ? $settings['seo_description']->value : ''}}">
<meta name="keywords" content="{{ isset($settings,$settings['seo_keyword']) ? $settings['seo_keyword']->value : ''}}">

@if (array_key_exists('meta-tag', View::getSections()))
    @yield('meta-tag')
@else
    @php
        $title = isset($settings,$settings['site_name']) ? $settings['site_name']->value : 'CAA Cambodia';
        $description = isset($settings,$settings['seo_description']) ? $settings['seo_description']->value : '';
        $image = isset($settings['logo']) ? $settings['logo']->getFirstMedia('images') : '';
        $url = url()->full();
        $siteName = isset($settings,$settings['site_name']) ? $settings['site_name']->value : '';
    @endphp

    <meta property="og:title" content="{{ $title }}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ $image->getUrl() }}" />
    <meta property="og:url" content="{{ $url }}" />
    <meta property="og:description" content="{{ $description }}" />
    <meta property="og:site_name" content="{{ $siteName }}">

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="{{ $title }}" />
    <meta name="twitter:description" content="{{ $description }}" />
    <meta name="twitter:image" content="{{ $image->getUrl() }}" />
@endif

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">
<link rel="stylesheet" href="{{asset('client/css/all.min.css')}}?v=3">
