@extends('client.layouts.master')

@section('title')
     {{ $event->title }} {{ isset($settings,$settings['site_name']) ? '- '.$settings['site_name']->value : ''}}
@endsection

@section('meta-tag')
    @php
        $title = $event->title;
        $description = html_entity_decode(mb_strimwidth(strip_tags($event->description), 0, 500, "..."));
        $image = $event->getFirstMediaUrl('images', 'thumb');
        $url = url()->full();
        $siteName = isset($settings,$settings['site_name']) ? $settings['site_name']->value : '';
    @endphp

    <!-- For Facebook -->
    <meta property="og:title" content="{{ $title }}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ $image }}" />
    <meta property="og:url" content="{{ $url }}" />
    <meta property="og:description" content="{{ $description }}" />
    <meta property="og:site_name" content="{{ $siteName }}">

    <!-- For Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="{{ $title }}" />
    <meta name="twitter:description" content="{{ $description }}" />
    <meta name="twitter:image" content="{{ $image }}" />

@endsection

@section('content')
@php $image = isset($settings,$settings['event_banner']) ? $settings['event_banner']->getMedia('images')->first() : null @endphp
<div class="page-banner" style="background-image: url({{ isset($image) && $image ? $image->getUrl() : '/client/images/img-page-header-2.jpg' }})">
    <div class="overlay">
        <div class="title">{{ trans('client.event_menu') }}</div>
    </div>
</div>
<div id="event-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8 main-section pb-3">
                <div class="card p-4">
                    <div class="title">{{ $event->title }}</div>
                    <hr class="mb-3">
                    <div class="social-share-button mb-3">
                        <a target="_BLANK" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a target="_BLANK" class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url()->full() }}">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a target="_BLANK" class="twitter" href="https://twitter.com/intent/tweet?text={{ url()->full() }}">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a target="_BLANK" class="telegram" href="https://telegram.me/share/url?url={{ url()->full() }}">
                            <i class="fab fa-telegram"></i>
                        </a>
                        <a target="_BLANK" class="whatsapp" href="https://api.whatsapp.com/send?text={{ url()->full() }}">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                    <div class="description">
                        {!! $event->description !!}
                    </div>
                </div>

            </div>
            <div class="col-md-4 side-section">
                <div class="card pt-4 pb-4 pl-3 pr-3">
                    <div class="heading">
                        {{ trans('client.latest_event_label') }}
                    </div>
                    @if(isset($events) && $events->count() > 0)
                        @foreach($events as $row)
                        <div class="content pt-2 pb-2 border-bottom">
                            <a href="{{ route('client.event-detail',$row->id) }}" class="text-decoration-none item">{{ $row->title }}<i class="fa fa-link"></i></a>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>

</script>
@endsection
