@extends('client.layouts.master')

@section('title')
    Event {{ isset($settings,$settings['site_name']) ? '- '.$settings['site_name']->value : ''}}
@endsection

@section('content')
@php $image = isset($settings,$settings['event_banner']) ? $settings['event_banner']->getMedia('images')->first() : null @endphp
<div class="page-banner" style="background-image: url({{ isset($image) && $image ? $image->getUrl() : '/client/images/img-page-header-2.jpg' }})">
    <div class="overlay">
        <div class="title">{{ trans('client.event_menu') }}</div>
    </div>
</div>
<div id="event">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-lg-2 col-lg-8">
                @if((isset($events) && $events->count() > 0) || (isset($latestEvent) && $latestEvent->count() > 0))
                <div class="card shadow border-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 item main pb-4">
                                <div class="row">
                                    <div class="col-sm-6 col-12 px-2">
                                        @php $image = $latestEvent->getFirstMediaUrl('images', 'thumb') @endphp
                                        <img class="image" src="{{ $image }}" alt="{{ $latestEvent->title }}">
                                    </div>
                                    <div class="col-sm-6 col-12 px-2">
                                        <div class="info">
                                            <div class="title"><a class="text-decoration-none" href="{{route('client.event-detail',$latestEvent->id)}}">{{ $latestEvent->title }}</a></div>
                                            <div class="date"><i class="fa fa-clock"></i> {{ date('d/m/Y',strtotime($latestEvent->created_at)) }}</div>
                                            <div class="description">{{ html_entity_decode(mb_strimwidth(strip_tags($latestEvent->description), 0, 500, "...")) }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="infinite-scroll">
                            <div class="row justify-content-center">
                                @foreach($events as $key => $row)
                                    <div class="col-xl-6 col-lg-12 col-md-6 item pb-4">
                                        <div class="row">
                                            <div class="col-sm-6 col-12 px-2">
                                                @php $image = $row->getFirstMediaUrl('images', 'thumb') @endphp
                                                <img class="image" src="{{ $image }}" alt="{{ $row->title }}">
                                            </div>
                                            <div class="col-sm-6 col-12 px-2">
                                                <div class="info">
                                                    <div class="title"><a class="text-decoration-none" href="{{route('client.event-detail',$row->id)}}">{{ $row->title }}</a></div>
                                                    <div class="date"><i class="fa fa-clock"></i> {{ date('d/m/Y',strtotime($row->created_at)) }}</div>
                                                    <div class="description">{{ html_entity_decode(mb_strimwidth(strip_tags($row->description), 0, 500, "...")) }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                {{$events->links()}}
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-lg-12">
                                <div class="view-more pt-3 pb-4">
                                    <a href="#">View More</a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-2 sidebar d-none d-lg-block">
                <div class="card item shadow border-0">
                    <div class="card-header text-center">
                        {{ trans('client.today_view_label') }}
                    </div>
                    <div class="card-body">
                        @if(isset($settings,$settings['facebook']) && !empty($settings['facebook']))
                            <img width="35px;" src="{{asset('client/images/icon/ic-person.svg')}}" alt="Users">
                        @endif
                        {{ $visitors }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>

    // Scroll
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<div class="text-center"><img width="100px" class="center-block " src="/client/images/loading.gif" alt="Loading..." /></div>', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

</script>
@endsection
