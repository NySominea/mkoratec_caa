@extends('client.layouts.master')

@section('title')
    {{ isset($settings,$settings['site_name']) ? $settings['site_name']->value : 'Homepage'}}
@endsection

@section('content')
<div id="homepage">

    {{-- Start Banner --}}
    <div class="banner" id="banner">
        <div class="banner-slide">
            <div id="slide" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
                <ul class="carousel-indicators">
                    @foreach($banners as $key => $row)
                    <li data-target="#slide" data-slide-to="{{$key}}"></li>
                    @endforeach
                </ul>
                <div class="carousel-inner">
                    @foreach($banners as $key => $row)
                    @php $image = $row->getFirstMediaUrl('images','thumb') @endphp
                    <div class="carousel-item {{ $key == 0 ? 'active' : ''}}" style="background-image: url({{ $image }})">
                        <div class="overlay" style="background:none">
                            <div class="carousel-caption">
                                <div class="title my-3">
                                    <h1 class="display-4 text-uppercase font-weight-bold">{{ $row->title }}</h1>
                                </div>
                                <div class="description mx-auto">{!! $row->description !!}</div>
                                @if($row->link)
                                <div class="learn-more"><a href="{{$row->link}}" class="btn btn-main mt-3">{{ trans('client.learn_more_button') }}</a></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    {{-- End Banner  --}}

    {{-- Start Event --}}
    @if(isset($events) && $events->count() > 0)
    <div class="event mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="card content shadow border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="section-title">{{ trans('client.event_label') }}</div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($events as $key => $row)
                                @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                                <div class="col-lg-12 col-md-12 col-sm-6 item py-3">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <img class="image" src="{{ $image }}" alt="{{ $row->title }}">
                                        </div>
                                        <div class="col-lg-7 col-md-7">
                                            <div class="info">
                                                <div class="title"><a class="text-decoration-none" href="{{route('client.event-detail',$row->id)}}">{{ $row->title }}</a></div>
                                                <div class="description">{{ html_entity_decode(mb_strimwidth(strip_tags($row->description), 0, 600, "...")) }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="view-more">
                                        <a href="{{route('client.event')}}">{{ trans('client.view_more_button') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 sidebar d-none d-lg-block">
                    <div class="card item shadow border-0">
                        <div class="card-header text-center">
                            {{ trans('client.today_view_label') }}
                        </div>
                        <div class="card-body">
                            @if(isset($settings,$settings['facebook']) && !empty($settings['facebook']))
                                <img width="35px;" src="{{asset('client/images/icon/ic-person.svg')}}" alt="Users">
                            @endif
                            {{ $visitors }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- End Event --}}

    {{-- Start Our Mission --}}
    <div class="our-mission">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="card shadow border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="section-title">{{ trans('client.mission_label') }}</div>
                                </div>
                            </div>
                            <div class="row content">
                                <div class="col-md-6 pr-md-0">
                                    <div class="item first" style="background-image:url('/client/images/img-mission-1.jpg')">
                                        <div class="overlay">
                                            <div class="card-title align-items-center d-flex justify-content-center">
                                                {{ isset($principleCategories[2]) && $principleCategories[2]->principles->count() > 0 ? $principleCategories[2]->principles[0]->description : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-md-0">
                                    <div class="item second" style="background-image:url('/client/images/img-mission-2.jpg')">
                                        <div class="overlay">
                                            <div class="card-title align-items-center d-flex justify-content-center">
                                                    {{ isset($principleCategories[2]) && $principleCategories[2]->principles->count() > 1 ? $principleCategories[2]->principles[1]->description : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="item" style="background-image:url('/client/images/img-mission-3.jpg')"></div>
                                </div>
                            </div>

                            <div class="row content row-eq-height mt-4">
                                @if(isset($principleCategories[2]) && $principleCategories[2]->principles->count() > 0)
                                    @php $missions = $principleCategories[2]->principles->slice(2);  @endphp
                                    @foreach($missions as $index => $row)
                                        <div class="item-container col-xl-6 col-lg-6 col-md-6">
                                            <div class="card item h-100">
                                                <div class="card-header text-center">
                                                    {{ $index - 1}}
                                                </div>
                                                <div class="card-body">
                                                    {{-- <h5 class="card-title">{{ $row->title }}</h5> --}}
                                                    <p class="card-text">{{ $row->description }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    {{ isset($settings,$settings['site_name']) ? $settings['site_name']->value : '' }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- End Our Mission --}}


    {{-- Start Our Vision --}}
    <div class="our-vision mt-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="card content shadow border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="section-title">{{ trans('client.vision_label') }}</div>
                                </div>
                            </div>
                            <div class="row row-eq-height">
                                @if(isset($principleCategories[1]) && $principleCategories[1]->principles->count() > 0)
                                    @foreach($principleCategories[1]->principles as $index => $row)
                                        <div class="item-container col-xl-12 col-lg-12 col-md-12">
                                            <div class="card item h-100">
                                                {{-- <div class="card-header text-center">
                                                    {{ $index + 1}}
                                                </div> --}}
                                                <div class="card-body">
                                                    {{-- <h5 class="card-title">{{ $row->title }}</h5> --}}
                                                    <p class="card-text">{{ $row->description }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    {{ isset($settings,$settings['site_name']) ? $settings['site_name']->value : '' }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-2 sidebar d-none d-lg-block">
                    <div class="card item shadow border-0">
                        <div class="card-header text-center">
                            {{ trans('client.today_view_label') }}
                        </div>
                        <div class="card-body">
                            @if(isset($settings,$settings['facebook']) && !empty($settings['facebook']))
                                <img width="35px;" src="{{asset('client/images/icon/ic-person.svg')}}" alt="Users">
                            @endif
                            {{ $visitors }}
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- End Our Vision --}}

    {{-- Start Parnter --}}
    @if(isset($partners) && $partners->count() > 0)
    <div class="partner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section-title">{{ trans('client.partner_label') }}</div>
                </div>
            </div>
            <div class="row mb-5 mt-3">
                <div class="col-lg-10 offset-lg-1">
                    <div class="owl-carousel owl-theme">
                        @foreach($partners as $row)
                            @php  $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                            @if($image)
                                <a href="{{$row->link}}" target="_BLANK"><img class="" src="{{ $image }}" alt="{{$row->name}}"></a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- End Partner --}}

    {{-- Start Gallery --}}
    <div class="gallery">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section-title">{{ trans('client.gallery_label') }}</div>
                </div>
            </div>
            <div class="row justify-content-center">
                @foreach($galleries as $index => $row)
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-6">
                    @php $image = $row->getFirstMediaUrl('images', 'thumb') @endphp
                    <div class="card">
                        @if($image)
                        <img src="{{$image}}" class="card-img-top rounded" alt="{{$row->name}}">
                        @endif
                        <div class="card-body">
                            <h5 class="card-title mb-0 text-center">
                                <a href="{{route('client.gallery')}}?province={{$row->province?$row->province->name:''}}&location={{$row->location}}" class="text-decoration-non text-secondary">
                                    <div class="title">{{$row->location}}</div>
                                </a>
                            </h5>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
            <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="view-more pt-0 pb-5">
                        <a href="{{route('client.gallery')}}">{{ trans('client.view_more_button') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- End Partner --}}
</div>
@endsection

@section('custom-js')

@endsection
