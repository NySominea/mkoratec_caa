@extends('client.layouts.master')

@section('title')
    Gallery {{ isset($settings,$settings['site_name']) ? '- '.$settings['site_name']->value : ''}}
@endsection

@section('content')
@php $image = isset($settings,$settings['gallery_banner']) ? $settings['gallery_banner']->getMedia('images')->first() : null @endphp
<div class="page-banner" style="background-image: url({{ isset($image) && $image ? $image->getUrl() : '/client/images/img-page-header-2.jpg' }})">
    <div class="overlay">
        <div class="title">{{ trans('client.gallery_menu') }}</div>
    </div>
</div>
<div id="gallery">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-3 mb-3">
                <div class="card shadow border-0">
                    <div class="card-body">
                        <h6 class="text-color" style="font-size:12px;">* {{ trans('client.province_placeholder') }}</h6>
                        {{ Form::model($provinces,['route' => 'client.gallery', 'method' => 'GET']) }}
                        <div class="form-group">
                            {!! Form::select('province',$provinces,request()->province?:null,['class' => 'form-control custom-select block']) !!}
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-secondary-color btn-sm">{{ trans('client.select_button') }}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-9">
                <div class="card shadow border-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="section-title" style="min-width:100%;">
                                    @if(request()->province && request()->province != 'all' && !request()->location)
                                        {{ trans('client.all_album_in_province',['province' => request()->province]) }}
                                    @elseif(request()->province && request()->location)
                                        {{ trans('client.all_photo_of_location_in_province',['location' => request()->location, 'province' => request()->province]) }}
                                    @else
                                        {{ trans('client.all_album_label') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if(request()->location)
                            <div class="infinite-scroll">
                                <div class="row justify-content-center">
                                    @foreach($galleries as $index => $row)
                                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-6 p-2">
                                        <div class="item">
                                            <a href="{{$row->getUrl()}}" data-toggle="lightbox" data-gallery="example-gallery">
                                                <img src="{{$row->getUrl('thumb')}}" class="img-fluid">
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                    {{ $galleries->links() }}
                                </div>
                            </div>
                        @else
                            <div class="row justify-content-center">
                                @foreach($galleries as $index => $row)
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 p-2">
                                    @php $image = $row->getFirstMediaUrl('images', 'thumb') @endphp
                                    <div class="card ">
                                        @if($image)
                                        <img src="{{$image}}" class="card-img-top" alt="{{$row->name}}">
                                        @endif
                                        <div class="card-body">
                                            <h5 class="card-title mb-0 text-center">
                                                <a href="{{route('client.gallery')}}?province={{$row->province?$row->province->name:''}}&location={{$row->location}}" class="text-decoration-non text-secondary">
                                                    <div class="title">{{$row->location}}</div>
                                                </a>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        @endif

                        @if($galleries->count() == 0)
                            <div class="col-12 text-center">
                                {{ trans('client.no_content_label') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-2 sidebar d-none d-lg-block">
                <div class="card item shadow border-0">
                    <div class="card-header text-center">
                        {{ trans('client.today_view_label') }}
                    </div>
                    <div class="card-body">
                        @if(isset($settings,$settings['facebook']) && !empty($settings['facebook']))
                            <img width="35px;" src="{{asset('client/images/icon/ic-person.svg')}}" alt="Users">
                        @endif
                        {{$visitors}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>

    // Scroll
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<div class="text-center"><img width="100px" class="center-block " src="/client/images/loading.gif" alt="Loading..." /></div>', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

</script>
@endsection
