@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Partners</h4>
                @can('add-new-partner')
                <div class="heading-elements">
                    <a href="{{route('admin.partners.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Logo</th>
                                <th>Name</th>
                                <th style="min-width:100px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($partners) && $partners->count() > 0)
                                        @foreach($partners as $key => $row)
                                        @php $image = $row->getMedia('images')->first(); @endphp
                                        <tr>
                                            <td>{{($partners->perPage() * ($partners->currentPage() - 1)) + $key + 1}}</td>
                                            <td>
                                                <img width="70px" src="{{$image ? $image->getUrl() : ''}}" alt="">
                                            </td>
                                            <td>{!! $row->name !!}</td>
                                            <td class="group-btn-action">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                    <a href="{{ route('admin.partners.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                    @can('partner-modification')
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.partners.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="4">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($partners) && $partners->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $partners->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$partners->firstItem()}} to {{$partners->lastItem()}}
                    of  {{$partners->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
