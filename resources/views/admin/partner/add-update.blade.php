@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($partner) ? 'Update' : 'Add' }} Partner</h4>
            </div>

            @if(isset($partner))
            {{ Form::model($partner,['route' => ['admin.partners.update',$partner->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.partners.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Partner Logo (Aspec Ratio 1:1) <span class="text-danger">*</span></label>
                                @component('common.single_dropzone',['id' => "partnerDropzone",'object' => isset($partner) ? $partner : null, 'width' => 150, 'height' => 150])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Title</label>
                                {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Enter partner name']) !!}
                                @component('common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Link</label>
                                {!! Form::text('link',null,['class' => 'form-control', 'placeholder' => 'Enter link']) !!}
                                @component('common.error_helper_text',['key' => "link"])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                @if(isset($partner)))
                    @can('partner-modification')
                    <div class="card-footer">
                        <a href="{{route('admin.partners.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else 
                    @can('add-new-partner')
                    <div class="card-footer">
                        <a href="{{route('admin.partners.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection