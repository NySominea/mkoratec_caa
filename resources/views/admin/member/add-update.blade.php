@extends('admin.layouts.master')

@section('page-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($member) ? 'Update' : 'Add' }} Member</h4>
            </div>

            @if(isset($member))
            {{ Form::model($member,['route' => ['admin.members.update',$member->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.members.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Profile Image (Aspec Ratio 1:1) <span class="text-danger">*</span></label>
                                @component('common.single_dropzone',['id' => "memberDropzone",'object' => isset($member) ? $member : null, 'width' => 150, 'height' => 150])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput"> Name <span class="text-danger">*</span> </label>
                                {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Enter name']) !!}
                                @component('common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Position</label>
                                {!! Form::text('position',null,['class' => 'form-control', 'placeholder' => 'Enter position']) !!}
                                @component('common.error_helper_text',['key' => "position"])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                @if(isset($partner)))
                    @can('member-modification')
                    <div class="card-footer">
                        <a href="{{route('admin.pamembersrtners.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else 
                    @can('add-new-member')
                    <div class="card-footer">
                        <a href="{{route('admin.members.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection