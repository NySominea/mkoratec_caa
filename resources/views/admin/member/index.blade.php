@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Members</h4>
                @can('add-new-member')
                <div class="heading-elements">
                    <a href="{{route('admin.members.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Profile</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th style="min-width:100px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($members) && $members->count() > 0)
                                        @foreach($members as $key => $row)
                                        @php $image = $row->getMedia('images')->first(); @endphp
                                        <tr>
                                            <td>{{($members->perPage() * ($members->currentPage() - 1)) + $key + 1}}</td>
                                            <td>
                                                <img width="70px" class="rounded-circle" src="{{$image ? $image->getUrl() : ''}}" alt="">
                                            </td>
                                            <td>{!! $row->name !!}</td>
                                            <td>{!! $row->position !!}</td>
                                            <td class="group-btn-action">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                    <a href="{{ route('admin.members.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                    @can('member-modification')
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.members.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($members) && $members->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $members->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$members->firstItem()}} to {{$members->lastItem()}}
                    of  {{$members->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
