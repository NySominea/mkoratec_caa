@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Settings</h4>
            </div>

            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="base-general" data-toggle="tab" aria-controls="general" href="#general" role="tab" aria-selected="true">
                                        General
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-social" data-toggle="tab" aria-controls="social" href="#social" role="tab" aria-selected="true">
                                        Social Link
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-page-banner" data-toggle="tab" aria-controls="page-banner" href="#page-banner" role="tab" aria-selected="true">
                                        Page Banner
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-page-description" data-toggle="tab" aria-controls="page-description" href="#page-description" role="tab" aria-selected="true">
                                        Page Description
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-seo" data-toggle="tab" aria-controls="seo" href="#seo" role="tab" aria-selected="true">
                                        SEO
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-bank-info" data-toggle="tab" aria-controls="bank-info" href="#bank-info" role="tab" aria-selected="true">
                                        Bank Info
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                <div class="tab-pane active" id="general" role="tabpanel" aria-labelledby="base-general">
                                    @include('admin.setting.general') 
                                </div>
                                <div class="tab-pane" id="social" role="tabpanel" aria-labelledby="base-social">
                                    @include('admin.setting.social-link') 
                                </div>
                                <div class="tab-pane" id="page-banner" role="tabpanel" aria-labelledby="base-page-banner">
                                    @include('admin.setting.page-banner') 
                                </div>
                                <div class="tab-pane" id="page-description" role="tabpanel" aria-labelledby="base-page-description">
                                    @include('admin.setting.page-description') 
                                </div>
                                <div class="tab-pane" id="seo" role="tabpanel" aria-labelledby="base-seo">
                                    @include('admin.setting.seo') 
                                </div>
                                <div class="tab-pane" id="bank-info" role="tabpanel" aria-labelledby="base-bank-info">
                                    @include('admin.setting.bank-info') 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection