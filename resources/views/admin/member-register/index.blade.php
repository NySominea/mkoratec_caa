@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Registered Members</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Photo</th>
                                <th style="min-width:140px">User Name</th>
                                <th>ID No</th>
                                <th style="min-width:140px">Phone</th>
                                <th>Email</th>
                                <th>Registered At</th>
                                <th style="min-width:100px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($members) && $members->count() > 0)
                                        @foreach($members as $key => $row)
                                        <tr>
                                            <td>{{($members->perPage() * ($members->currentPage() - 1)) + $key + 1}}</td>
                                            <td>
                                                @php $image = $row->getFirstMedia('images');@endphp
                                                @if($image)
                                                    <img width="50px;" src="{{$image->getUrl()}}">
                                                @endif
                                            </td>
                                            <td>{!! $row->en_name !!} - {!! $row->kh_name !!}</td>
                                            <td>{!! $row->id_number !!}</td>
                                            <td>{!! $row->phone !!}</td>
                                            <td>{!! $row->email !!}</td>
                                            <td>{{ $row->created_at->format('d F Y') }}</td>
                                            <td class="group-btn-action">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                    @can('view-register-user')
                                                    <button class="btn btn-outline-success preview"
                                                        data-popup="tooltip" title="" data-placement="bottom" data-original-title="Preview"
                                                        data-toggle="modal" data-target="#preview_modal"
                                                        data-route="{{route('admin.member-registers.show',$row->id)}}"><i class="ft-eye"></i></a></button>
                                                    @endcan

                                                    @can('download-register-user')
                                                    <a href="{{route('admin.member-registers.download',$row->id)}}" target="_BLANK" class="btn btn-outline-primary"><i class="ft-download"></i></a>
                                                    @endcan

                                                    @can('delete-register-user')
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.member-registers.destroy',$row->id)}}"
                                                        data-toggle="tooltip" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="ft-trash-2"></i>
                                                    </button>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($members) && $members->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $members->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$members->firstItem()}} to {{$members->lastItem()}}
                    of  {{$members->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection


@section('custom-css')
<style>
    #preview_modal table td{
        padding: 5px 0px;
        vertical-align: top;
    }
    #preview_modal table tr td:first-child{
        min-width: 150px;
    }
    #preview_modal table tr td:nth-child(2){
        width: 20px;
    }
</style>
@endsection
<div id="preview_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title">Preview</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body px-5">
                <div class="card-header">
                    <div class="text-center">
                        <img width="150px" src="" alt="" class="photo">
                    </div>
                    <h3 class="text-center"><strong class="en_name"></strong></h3>
                    <h5 class="text-center">Registered Date : <span class="created_at"></span></h5>
                </div>
                <table class="w-100" id="table">
                    <tbody>
                        <tr><td class="">English Name </td><td>:</td><td class="en_name"></td></tr>
                        <tr><td class="">Khmer Name No </td><td>:</td><td class="kh_name"></td></tr>
                        <tr><td class="">Gender</td><td>:</td><td class="gender"></td></tr>
                        <tr><td class="">Age </td><td>:</td><td class="age"></td></tr>
                        <tr><td class="">Address </td><td>:</td><td class="address"></td></tr>
                        <tr><td class="">ID Number </td><td>:</td><td class="id_no"></td></tr>
                        <tr><td class="">Education </td><td>:</td><td class="education"></td></tr>
                        <tr><td class="">Languages </td><td>:</td><td class="language"></td></tr>
                        <tr><td class="">Position </td><td>:</td><td class="position"></td></tr>
                        <tr><td class="">Phone </td><td>:</td><td class="phone"></td></tr>
                        <tr><td class="">Email </td><td>:</td><td class="email"></td></tr>
                        <tr><td class="">Against Humanity </td><td>:</td><td class="against_humanity"></td></tr>
                        <tr><td class="">Facebook </td><td>:</td><td class="facebook"></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@section('custom-js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $(document).ready(function(){
            if($('#preview_modal').length > 0){
            $('.preview').click(function(){
                var en_name = $('#preview_modal .en_name');
                var kh_name = $('#preview_modal .kh_name');
                var gender = $('#preview_modal .gender');
                var age = $('#preview_modal .age');
                var address = $('#preview_modal .address');
                var id_no = $('#preview_modal .id_no');
                var education = $('#preview_modal .education');
                var language = $('#preview_modal .language');
                var position = $('#preview_modal .position');
                var phone = $('#preview_modal .phone');
                var email = $('#preview_modal .email');
                var facebook = $('#preview_modal .facebook');
                var against_humanity = $('#preview_modal .against_humanity');
                var created_at = $('#preview_modal .created_at');
                var photo = $('#preview_modal .photo');
                en_name.text('');
                kh_name.text('');
                gender.text('');
                age.text('');
                address.text('');
                id_no.text('');
                education.text('');
                language.text('');
                position.text('');
                phone.text('');
                email.text('');
                created_at.text('');
                against_humanity.text('');
                photo.removeClass('hidden')
                photo.attr('src','')
                $.ajax({
                    type:'GET',
                    url:$(this).data('route'),
                    dataType: "JSON",
                    data: {},
                    success:function(data){
                        if(data.success){
                            en_name.text(data.member.en_name);
                            kh_name.text(data.member.kh_name);
                            gender.text(data.member.gender);
                            age.text(data.member.age);
                            address.text(data.member.address);
                            id_no.text(data.member.id_number);
                            education.text(data.member.education);
                            language.text(data.member.language);
                            position.text(data.member.position);
                            phone.text(data.member.phone);
                            email.text(data.member.email);
                            facebook.text(data.member.facebook);
                            created_at.text(data.member.created_at);
                            against_humanity.text(data.member.against_humanity == 1 ? 'Yes' : 'No');

                            if(data.member.photo != ''){
                                photo.attr('src',data.member.photo)
                            }else{
                                photo.addClass('hidden')
                            }

                        }else{
                            swalOnResult('Error While Loading Information!','error');
                        }
                    },
                    error:function(){
                        swalOnResult('Error While Loading Information!','error');
                    }
                });
            });

        }
        })
    </script>
@endsection
