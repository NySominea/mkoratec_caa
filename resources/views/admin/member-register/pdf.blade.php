<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Battambang&display=swap" rel="stylesheet">
    <style>
        #pdf-view .table td{
            font-size: 18px;
            vertical-align: top;
            padding: 10px 0px;
        }
        #pdf-view .table tr td:first-child{
            width: 150px;
        }
        #pdf-view .table tr td:nth-child(2){
            width: 20px;
        }
        .en-name {font-family: 'Battambang', cursive; line-height: 12px;}
    </style>
</head>
<body>
    <div class="row" id="pdf-view">
        <div class="col-md-12">            
            <div class="card">
                <div class="card-content">
                    <div style="text-align:center">
                        {{-- <h3 class="card-title text-center">Member Information</h3> --}}
    
                        @if($member->getFirstMedia('images')) 
                        <div class="text-center">
                            <img width="100px" src="{{$member->getFirstMedia('images')->getUrl()}}" alt="">
                        </div>
                        @endif
                        <h2 class="text-center mt-2 mb-2"><strong>{{$member->en_name}}</strong></h2>
                        <h4 class="text-center">Registered Date : {{$member->created_at->format('d F Y')}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                        <tr>
                                            <td>English Name</td>
                                            <td>:</td>
                                            <td>{{ $member->en_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>Khmer Name</td>
                                            <td>:</td>
                                            <td class="en-name">{{ $member->kh_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td>:</td>
                                            <td>{{ $member->gender }}</td>
                                        </tr>
                                        <tr>
                                            <td>Age</td>
                                            <td>:</td>
                                            <td>{{ $member->age }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>:</td>
                                            <td>{{ $member->address }}</td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td>:</td>
                                            <td>{{ $member->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>{{ $member->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>ID Number</td>
                                            <td>:</td>
                                            <td>{{ $member->id_number }}</td>
                                        </tr>
                                        <tr>
                                            <td>Education</td>
                                            <td>:</td>
                                            <td>{{ $member->education }}</td>
                                        </tr>
                                        <tr>
                                            <td>Languages</td>
                                            <td>:</td>
                                            <td>{{ implode(', ',$member->language) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Position</td>
                                            <td>:</td>
                                            <td>{{ $member->position }}</td>
                                        </tr>
                                        <tr>
                                            <td>Against Humanity</td>
                                            <td>:</td>
                                            <td>{{ $member->against_humanity == 1 ? 'Yes' : 'No' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Facebook</td>
                                            <td>:</td>
                                            <td>{{ $member->facebook }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>


