@extends('admin.layouts.master')

@section('page-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($gallery) ? 'Update' : 'Add' }} Gallery</h4>
            </div>

            @if(isset($gallery))
            {{ Form::model($gallery,['route' => ['admin.galleries.update',$gallery->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.galleries.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset class="form-group">
                                <label for="basicInput"> Province / City </label>
                                {!! Form::select('province_id',$provinces,null,['class' => 'form-control custom-select block']) !!}
                                @component('common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Location <span class="text-danger">*</span></label>
                                {!! Form::text('location',null,['class' => 'form-control', 'placeholder' => 'Enter location']) !!}
                                @component('common.error_helper_text',['key' => "location"])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label>Album Thumbnail (Aspec Ratio 3:2) <span class="text-danger">*</span></label>
                                @component('common.single_dropzone',['id' => "galleryDropzone",'object' => isset($gallery) ? $gallery : null, 'width' => 260, 'height' => 160])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label>Album Images (Aspec Ratio 3:2) <span class="text-danger">*</span></label>
                                @component('common.multi_dropzone',['id' => "galleryMulitDropzone",'object' => isset($gallery) ? $gallery : null, 'width' => 180, 'height' => 120])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>

                @if(isset($gallery))
                    @can('gallery-modification')
                    <div class="card-footer">
                        <a href="{{route('admin.galleries.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else
                    @can('add-new-gallery')
                    <div class="card-footer">
                        <a href="{{route('admin.galleries.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
