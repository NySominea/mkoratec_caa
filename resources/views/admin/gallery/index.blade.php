@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Gallery Albums</h4>
                @can('add-new-gallery')
                <div class="heading-elements">
                    <a href="{{route('admin.galleries.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Thumbnail</th>
                                <th>Location</th>
                                <th>Province/City</th>
                                <th style="min-width:100px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($galleries) && $galleries->count() > 0)
                                        @foreach($galleries as $key => $row)
                                        @php $image = $row->getMedia('images')->first(); @endphp
                                        <tr>
                                            <td>{{($galleries->perPage() * ($galleries->currentPage() - 1)) + $key + 1}}</td>
                                            <td>
                                                <img width="120px" height="80px" style="object-fit: cover" class="" src="{{$image ? $image->getUrl() : ''}}" alt="">
                                            </td>
                                            <td>{!! $row->location !!}</td>
                                            <td>{!! $row->province ? $row->province->name : '' !!}</td>
                                            <td class="group-btn-action">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                    <a href="{{ route('admin.galleries.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                    @can('gallery-modification')
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.galleries.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($galleries) && $galleries->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $galleries->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$galleries->firstItem()}} to {{$galleries->lastItem()}}
                    of  {{$galleries->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
