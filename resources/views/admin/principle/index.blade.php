@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Company Principle</h4>
                @can('add-new-principle')
                <div class="heading-elements">
                    <a href="{{route('admin.principles.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th style="min-width:200px">Title</th>
                                <th style="min-width:400px">Description</th>
                                <th style="min-width:150px">Category</th>
                                <th style="min-width:100px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($principles) && $principles->count() > 0)
                                        @foreach($principles as $key => $row)
                                        <tr>
                                            <td>{{($principles->perPage() * ($principles->currentPage() - 1)) + $key + 1}}</td>
                                            <td>{!! $row->title !!}</td>
                                            <td>{!! $row->description !!}</td>
                                            <td>{!! $row->category ? $row->category->name : '' !!}</td>
                                            <td class="group-btn-action">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                    <a href="{{ route('admin.principles.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                    @can('principle-modification')
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.principles.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($principles) && $principles->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $principles->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$principles->firstItem()}} to {{$principles->lastItem()}}
                    of  {{$principles->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
