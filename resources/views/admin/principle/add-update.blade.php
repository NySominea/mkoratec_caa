@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($principle) ? 'Update' : 'Add' }} Company Principle</h4>
            </div>

            @if(isset($principle))
            {{ Form::model($principle,['route' => ['admin.principles.update',$principle->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.principles.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg" role="tablist">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{ $key ==0 ? 'active' : '' }}" id="base-{{$lang}}" data-toggle="tab" aria-controls="{{$lang}}" href="#{{$lang}}" role="tab" aria-selected="true">
                                        <img width="30px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt="">
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                    <div class="tab-pane {{ $key == 0 ? 'active' : ''}}" id="{{$lang}}" role="tabpanel" aria-labelledby="base-{{$lang}}">
                                        <fieldset class="form-group">
                                            <label for="basicInput">Title <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""> </label>
                                            {!! Form::text('title['.$lang.']',isset($principle) ? $principle->getTranslation('title',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter title']) !!}
                                            @component('common.error_helper_text',['key' => "title.".$lang])@endcomponent
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label for="basicInput">Description <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""> <span class="text-danger">*</span></label>
                                            {!! Form::textarea('description['.$lang.']',isset($principle) ? $principle->getTranslation('description',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter description','rows' => '4']) !!}
                                            @component('common.error_helper_text',['key' => "description.".$lang])@endcomponent
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <fieldset class="form-group">
                                <label for="basicInput"> Principle Category </label>
                                {!! Form::select('category_id',$categories,null,['class' => 'form-control custom-select block']) !!}
                            </fieldset>
                        </div>
                    </div>
                </div>

                @if(isset($principle))
                    @can('principle-modification')
                    <div class="card-footer">
                        <a href="{{route('admin.principles.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else 
                    @can('add-new-principle')
                        <div class="card-footer">
                            <a href="{{route('admin.principles.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                            <button type="submit" class="btn btn-outline-primary">Save changes</button>
                        </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection