@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($event) ? 'Update' : 'Add' }} Event</h4>
            </div>

            @if(isset($event))
            {{ Form::model($event,['route' => ['admin.events.update',$event->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.events.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg" role="tablist">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{ $key ==0 ? 'active' : '' }}" id="base-{{$lang}}" data-toggle="tab" aria-controls="{{$lang}}" href="#{{$lang}}" role="tab" aria-selected="true">
                                        <img width="30px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt="">
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                    <div class="tab-pane {{ $key == 0 ? 'active' : ''}}" id="{{$lang}}" role="tabpanel" aria-labelledby="base-{{$lang}}">
                                        <fieldset class="form-group">
                                            <label for="basicInput">Title <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::text('title['.$lang.']',isset($event) ? $event->getTranslation('title',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter banner title']) !!}
                                            @component('common.error_helper_text',['key' => "title"])@endcomponent
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label for="basicInput">Description <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::textarea('description['.$lang.']',isset($event) ? $event->getTranslation('description',$lang) : null,['class' => 'form-control summernote', 'placeholder' => 'Enter banner description','rows' => '4']) !!}
                                            @component('common.error_helper_text',['key' => "description"])@endcomponent
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label>Event Image (Aspec Ratio 3:2) </label>
                                @component('common.single_dropzone',['id' => "eventDropzone",'object' => isset($event) ? $event : null, 'width' => 240, 'height' => 160])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                @if(isset($event))
                    @can('event-modification')
                    <div class="card-footer">
                        <a href="{{route('admin.events.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else
                    @can('add-new-event')
                    <div class="card-footer">
                        <a href="{{route('admin.events.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script src="{{asset('app-assets/custom/js/summernote.js')}}"></script>
@endsection
