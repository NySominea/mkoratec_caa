@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Events</h4>
                @can('add-new-event')
                <div class="heading-elements">
                    <a href="{{route('admin.events.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th style="min-width:100px;">Image</th>
                                <th style="min-width:250px">Title</th>
                                <th style="min-width:160px">Created At</th>
                                <th style="min-width:150px">Action</th>
                            </thead>
                            <tbody>
                                @forelse($events as $key => $row)
                                    @php $image = $row->getMedia('images')->first(); @endphp
                                    <tr>
                                        <td>{{($events->perPage() * ($events->currentPage() - 1)) + $key + 1}}</td>
                                        <td>
                                            @if($image) <img width="120px;" height="80px" style="object-fit: cover" src="{{$image->getUrl()}}" alt="{{$row->title}}"> @endif
                                        </td>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ \Carbon\Carbon::parse($row->created_at)->format('Y-m-d h:i A') }}</td>
                                        <td class="group-btn-action">
                                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                <a href="{{ route('admin.events.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                @can('event-modification')
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.events.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($events) && $events->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $events->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$events->firstItem()}} to {{$events->lastItem()}}
                    of  {{$events->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
