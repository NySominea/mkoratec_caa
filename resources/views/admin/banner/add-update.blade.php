@extends('admin.layouts.master')

@section('page-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($banner) ? 'Update' : 'Add' }} Banner</h4>
            </div>

            @if(isset($banner))
            {{ Form::model($banner,['route' => ['admin.banners.update',$banner->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.banners.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Banner Image (Aspec Ratio 5:2) <span class="text-danger">*</span></label>
                                @component('common.single_dropzone',['id' => "bannerDropzone",'object' => isset($banner) ? $banner : null, 'width' => 300, 'height' => 120])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg" role="tablist">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{ $key ==0 ? 'active' : '' }}" id="base-{{$lang}}" data-toggle="tab" aria-controls="{{$lang}}" href="#{{$lang}}" role="tab" aria-selected="true">
                                        <img width="30px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt="">
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                    <div class="tab-pane {{ $key == 0 ? 'active' : ''}}" id="{{$lang}}" role="tabpanel" aria-labelledby="base-{{$lang}}">
                                        <fieldset class="form-group">
                                            <label for="basicInput">Title <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::text('title['.$lang.']',isset($banner) ? $banner->getTranslation('title',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter banner title']) !!}
                                            @component('common.error_helper_text',['key' => "title"])@endcomponent
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label for="basicInput">Description <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::textarea('description['.$lang.']',isset($banner) ? $banner->getTranslation('description',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter banner description','rows' => '4']) !!}
                                            @component('common.error_helper_text',['key' => "description"])@endcomponent
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <fieldset class="form-group">
                                <label for="basicInput">Link</label>
                                {!! Form::text('link',null,['class' => 'form-control', 'placeholder' => 'Enter link']) !!}
                                @component('common.error_helper_text',['key' => "link"])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                @if(isset($banner))
                    @can('banner-modification')
                    <div class="card-footer">
                        <a href="{{route('admin.banners.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else
                    @can('add-new-banner')
                    <div class="card-footer">
                        <a href="{{route('admin.banners.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
