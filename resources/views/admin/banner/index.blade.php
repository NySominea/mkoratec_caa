@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Banners</h4>
                @can('add-new-banner')
                <div class="heading-elements">
                    <a href="{{route('admin.banners.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Banner</th>
                                <th style="min-width:200px">Title</th>
                                <th>Description</th>
                                <th style="min-width:100px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($banners) && $banners->count() > 0)
                                        @foreach($banners as $key => $row)
                                        @php $image = $row->getMedia('images')->first(); @endphp
                                        <tr>
                                            <td>{{($banners->perPage() * ($banners->currentPage() - 1)) + $key + 1}}</td>
                                            <td>
                                                <img width="300px" height="120px" style="object-fit: cover;" src="{{$image ? $image->getUrl() : ''}}" alt="">
                                            </td>
                                            <td>{!! $row->title !!}</td>
                                            <td>{{ $row->description }}</td>
                                            <td class="group-btn-action">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                    <a href="{{ route('admin.banners.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>

                                                    @canany(['banner-modification'])
                                                        <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.banners.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    @endcanany
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($banners) && $banners->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $banners->appends(Request::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$banners->firstItem()}} to {{$banners->lastItem()}}
                    of  {{$banners->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
