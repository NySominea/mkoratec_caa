@if(session('success'))
<div class="alert alert-success text-white alert-dismissible mb-2">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <span class="font-weight-semibold">Well done!</span> {{session('success')}}
</div>
@endif

