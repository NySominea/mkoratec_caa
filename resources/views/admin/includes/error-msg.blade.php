@if(session('error')) 
<div class="alert alert-danger alert-dismissible mb-2">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <span class="font-weight-semibold">Oh snap!</span> {{session('error')}}
</div>
@endif