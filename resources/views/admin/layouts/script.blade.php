<!-- BEGIN: Vendor JS-->
<script src="/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="/app-assets/vendors/js/charts/raphael-min.js"></script>
<script src="/app-assets/vendors/js/charts/morris.min.js"></script>
<script src="/app-assets/vendors/js/extensions/unslider-min.js"></script>
<script src="/app-assets/vendors/js/timeline/horizontal-timeline.js"></script>
<script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="/app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="/app-assets/js/core/app-menu.js"></script>
<script src="/app-assets/js/core/app.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="/app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
<script src="/app-assets/js/scripts/extensions/sweet-alerts.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>
    $(".nav-item a").each(function() {
    if ((window.location.href.indexOf($(this).attr('href'))) > -1) {
        $(this).parent().addClass('active');
    }
});
</script>
<!-- END: Page JS-->
<script src="/app-assets/custom/js/sidebar.js"></script>
<script src="/app-assets/custom/js/custom.js"></script>
@yield('custom-js')
