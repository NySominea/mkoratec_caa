<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @canany(['view-register-user','delete-register-user','download-register-user'])
            <li class=" nav-item"><a href="{{ route('admin.member-registers.index') }}"><i class="ft-users"></i><span class="menu-title" data-i18n="">Member Registration</span></a>
            </li>
            @endcanany

            @canany(['view-banner','banner-modification','add-new-banner'])
            <li class=" nav-item"><a href="{{ route('admin.banners.index') }}"><i class="ft-image"></i><span class="menu-title" data-i18n="">Banners</span></a>
            </li>
            @endcanany

            @canany(['view-event','event-modification','add-new-event'])
            <li class=" nav-item"><a href="{{ route('admin.events.index') }}"><i class="ft-grid"></i><span class="menu-title" data-i18n="">Events</span></a>
            </li>
            @endcanany

            @canany(['view-gallery','gallery-modification','add-new-gallery'])
            <li class=" nav-item"><a href="{{ route('admin.galleries.index') }}"><i class="ft-image"></i><span class="menu-title" data-i18n="">Galleries</span></a>
            </li>
            @endcanany

            @canany(['view-partner','partner-modification','add-new-partner'])
            <li class=" nav-item"><a href="{{ route('admin.partners.index') }}"><i class="ft-link"></i><span class="menu-title" data-i18n="">Partners</span></a>
            </li>
            @endcanany

            @canany(['view-member','member-modification','add-new-member'])
            <li class=" nav-item"><a href="{{ route('admin.members.index') }}"><i class="ft-users"></i><span class="menu-title" data-i18n="">Our Members</span></a>
            </li>
            @endcanany

            @canany(['view-principle','principle-modification','add-new-principle'])
            <li class=" nav-item"><a href="{{ route('admin.principles.index') }}"><i class="ft-clipboard"></i><span class="menu-title" data-i18n="">Company Principles</span></a>
            </li>
            @endcanany

            @canany(['view-setting','setting-modification'])
            <li class=" nav-item"><a href="{{ route('admin.settings.index') }}"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Company Setting</span></a>
            </li>
            @endcanany

            @canany(['view-language','language-modification'])
            <li class=" nav-item"><a href="{{ route('admin.languages.index') }}"><i class="ft-globe"></i><span class="menu-title" data-i18n="">Languages</span></a>
            </li>
            @endcanany

            @canany(['view-user','user-modification','add-new-user','view-role','role-modification','add-new-role'])
            <li class="nav-item has-sub"><a href="#"><i class="ft-unlock"></i><span class="menu-title" data-i18n="">Administrators</span></a>
                <ul class="menu-content" style="">
                    @canany(['view-user','user-modification','add-new-user'])
                    <li class=""><a class="menu-item" href="{{ route('admin.users.index') }}">Users</a>
                    </li>
                    @endcanany

                    @canany(['view-role','role-modification','add-new-role'])
                    <li class=""><a class="menu-item" href="{{ route('admin.roles.index') }}">Roles</a>
                    </li>
                    @endcanany
                </ul>
            </li>
            @endcanany
        </ul>
    </div>
</div>