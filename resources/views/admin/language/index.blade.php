@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Frontend Language</h4>
                @can('language-modification')
                <div class="heading-elements">
                    <button type="submit" form="language" class="btn btn-icon btn-success">
                        Save Changes
                    </button>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    <div class="table-responsive">
                        {{ Form::open(['route' => ['admin.languages.store'], 'method' => 'POST' ,'id' =>'language']) }}
                        @csrf
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>English</th>
                                <th>Khmer</th>
                                <th>Chinese</th>
                            </thead>
                            <tbody>
                                @if(isset($languages) && $languages->count() > 0)
                                @foreach($languages as $row)
                                <tr>
                                    <td>{{Form::text("languages[".$row->id."][en]",trans($row->group.'.'.$row->key,[],'en'),['class' => 'w-100 form-control'])}}</td>
                                    <td>{{Form::text("languages[".$row->id."][kh]",trans($row->group.'.'.$row->key,[],'kh'),['class' => 'w-100 form-control'])}}</td>
                                    <td>{{Form::text("languages[".$row->id."][zh]",trans($row->group.'.'.$row->key,[],'zh'),['class' => 'w-100 form-control'])}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
