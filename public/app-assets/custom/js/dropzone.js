
if($('.dropzone').length > 0){
    $('.dropzone').each(function(){
        if($(this).hasClass('multi')){
            multiDropzone($(this));
        }else{
            singleDropzone($(this));
        }
    });
}

function singleDropzone(_this){
    var myDropzone = _this;
    var inputSelector = $('#'+_this.data('input'));
    var width = _this.data('width');
    var height = _this.data('height');
    var actionSelector = $('#'+_this.data('action'));
    
    myDropzone.dropzone({
        acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
        maxFiles: 1,
        uploadMultiple: false,
        // addRemoveLinks: true,
        dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
        thumbnailWidth: width,
        thumbnailHeight: height,
    
        init: function(){
            var thisDropzone = this;
            if(actionSelector.val() == 'update'){
                var name = inputSelector.data('name');
                var size = inputSelector.data('size');
                var url  = inputSelector.data('url');
                if(url){
                    myDropzone.css('height','auto');
                    var mockFile = { name: name, size: size, accepted: true };  
                    
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, url);
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit("complete", mockFile);
                    thisDropzone.options.maxFiles = thisDropzone.options.maxFiles;
                    thisDropzone._updateMaxFilesReachedClass();
                } 
            }
            this.on('sending', function(file, xhr, formData){
                console.log('sending')
                formData.append("_token", $("input[name='_token']").val());
                formData.append("_method", "POST");
            })
            this.on("success", function(file, response){
                inputSelector.val(response.path);
            }),
            this.on("addedfile", function(event) {
                while (thisDropzone.files.length > thisDropzone.options.maxFiles) {
                    thisDropzone.removeFile(thisDropzone.files[0]);
                }
                myDropzone.css('height','auto');
            });
            this.on("removedfile", function(file){
                file.previewElement.remove();
                if(this.files.length < 1){
                    myDropzone.css('height','');
                }
            })
        } 
    })
}

function multiDropzone(_this){
    var myDropzone = _this;
    var inputSelector = $('#'+_this.data('input'));
    var deleteSelector = $('#'+_this.data('delete'));
    var width = _this.data('width');
    var height = _this.data('height');
    var actionSelector = $('#'+_this.data('action'));
    
    myDropzone.dropzone({
        acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
        uploadMultiple: true,
        addRemoveLinks: true,
        dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
        thumbnailWidth: width,
        thumbnailHeight: height,
    
        init: function(){
            var thisDropzone = this;
            if(actionSelector.val() == 'update'){
                var obj_id = inputSelector.data('model-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'GET',  
                    url:myDropzone.data('route-get-image'),
                    dataType: "JSON",
                    data: {id:obj_id,object:myDropzone.data('model'),collection:myDropzone.data('collection')},
                    success:function(data){
                        if(data.success){
                            myDropzone.css('height','auto');
                            Object.keys(data.images).forEach(function(key){
                                var mockFile = { name: data.images[key].name, size: data.images[key].size, accepted: true };  
                                
                                thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                                thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data.images[key].url);
                                thisDropzone.files.push(mockFile);
                                thisDropzone.emit("complete", mockFile);
                                thisDropzone._updateMaxFilesReachedClass();
                            });
                        }else{
                            
                        }
                    },
                    error:function(){
                    }
                });

                var name = inputSelector.data('name');
                var size = inputSelector.data('size');
                var url  = inputSelector.data('url');
                if(url){
                    myDropzone.css('height','auto');
                    var mockFile = { name: name, size: size, accepted: true };  
                    
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, url);
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit("complete", mockFile);
                    thisDropzone.options.maxFiles = thisDropzone.options.maxFiles;
                    thisDropzone._updateMaxFilesReachedClass();
                } 
            }
            this.on('sending', function(file, xhr, formData){
                console.log('sending')
                formData.append("_token", $("input[name='_token']").val());
                formData.append("_method", "POST");
            })
            this.on("success", function(file, response){
                var val = inputSelector.val();
                if(val == ''){
                    inputSelector.val(response.path);
                }else{
                    var a = val + ',' + response.path
                    inputSelector.val(a);
                }
            }),
            this.on("addedfile", function(event) {
                myDropzone.css('height','auto');
            });
            this.on("removedfile", function(file){
                file.previewElement.remove();
                if(this.files.length < 1){
                    thisDropzone.css('height','');
                }
                var val = deleteSelector.val();
                if(val == ''){
                    deleteSelector.val(file.name);
                }else{
                    var a = val + ',' + file.name
                    deleteSelector.val(a);
                }
            })
        } 
    })
}



