const mix = require('laravel-mix');

mix.styles([
    'public/client/vendor/css/animate.min.css',
    'public/client/vendor/css/lightbox.min.css',
    'public/client/vendor/css/owl.carousel.min.css',
    'public/client/vendor/css/owl.theme.default.min.css',
    'public/client/css/custom.css'
    ], 'public/client/css/all.min.css');

mix.js([
    'public/client/vendor/js/lightbox.min.js',
    'public/client/vendor/js/jscroll.min.js',
    'public/client/vendor/js/owl.carousel.min.js',
    'public/client/js/custom.js'
    ], 'public/client/js/all.min.js');
