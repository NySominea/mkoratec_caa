<?php

Route::namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function(){
        Route::get('/login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('/login', ['uses' => 'LoginController@login', 'as' => 'postLogin']);
        Route::post('/logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });
    
    Route::group(['middleware' => 'auth','prefix' => 'admin', 'as' => 'admin.'], function(){ 
        Route::get('/dashboard',['uses' => 'DashboardController@index', 'as' => 'dashboard']);
        
        Route::group(['middleware' => ['permission:view-banner|banner-modification|add-new-banner']], function () {
            Route::resource('banners','BannerController');
        });

        Route::group(['middleware' => ['permission:view-partner|partner-modification|add-new-partner']], function () {
            Route::resource('partners','PartnerController');
        });

        Route::group(['middleware' => ['permission:view-event|event-modification|add-new-event']], function () {
            Route::resource('events','EventController');
        });

        Route::group(['middleware' => ['permission:view-member|member-modification|add-new-member']], function () {
            Route::resource('members','MemberController');
        });
        Route::group(['middleware' => ['permission:view-gallery|gallery-modification|add-new-gallery']], function () {
            Route::resource('galleries','GalleryController');
        });
        Route::group(['middleware' => ['permission:view-principle|principle-modification|add-new-principle']], function () {
            Route::resource('principles','PrincipleController');
        });
        Route::group(['middleware' => ['permission:view-setting|setting-modification']], function () {
            Route::resource('settings','SettingController');
        });
        Route::group(['middleware' => ['permission:view-language|language-modification']], function () {
            Route::resource('languages','LanguageController');
        });
        Route::group(['middleware' => ['permission:download-register-user']], function () {
            Route::get('member-registers/pdf/{id}',['uses' => 'MemberRegisterController@downloadPdf', 'as' => 'member-registers.download']);
        });   
        Route::group(['middleware' => ['permission:view-register-user|delete-register-user|download-register-user']], function () {
            Route::resource('member-registers','MemberRegisterController');
        });
        Route::group(['middleware' => ['permission:view-user|user-modification|add-new-user']], function () {
            Route::resource('/users','UserController');
        });
        Route::group(['middleware' => ['permission:view-role|role-modification|add-new-role']], function () {
            Route::resource('/roles','UserRoleController');
        });
        Route::resource('/users/profile','EditProfileController',['as' => 'users']);

        //ajax upload image
        Route::post('/save-temp-image', ['as' => 'dropzoneSaveTempImage', 'uses' => 'ImageController@dropzoneSaveTempImage']);
        Route::post('/save-multi-temp-image', ['as' => 'dropzoneSaveMultiTempImage', 'uses' => 'ImageController@dropzoneSaveMultiTempImage']);
        Route::post('/save-summernote-image',['as' => 'summernoteSaveImage', 'uses' => 'ImageController@summernoteSaveImage']);
        Route::get('/get-image-list-by-object',['as' => 'getImageList', 'uses' => 'ImageController@getImageList']);
        Route::post('/delete-image-by-object',['as' => 'deleteDropzoneImage', 'uses' => 'ImageController@deleteDropzoneImage']);
    });  
});

Route::group(['namespace' => 'Client','as' =>'client.'],function(){
    Route::get('/language/set-locale/{locale}',['uses' => 'LanguageController@index', 'as' => 'set-locale']);
    Route::get('/',['uses' => 'FrontendController@index','as' => 'homepage']);
    Route::get('/galleries',['uses' => 'FrontendController@gallery', 'as' => 'gallery']);
    Route::get('/events',['uses' => 'FrontendController@event', 'as' => 'event']);
    Route::get('/events/{id}',['uses' => 'FrontendController@eventDetail', 'as' => 'event-detail']);
    Route::get('/about-us',['uses' => 'FrontendController@aboutUs', 'as' => 'about']);
    Route::get('/register',['uses' => 'FrontendController@registerForm', 'as' => 'register']);
    Route::post('/register',['uses' => 'FrontendController@register', 'as' => 'register']);
});
